EXECUTABLE=qpuengine
BUILDDIR=build
OBJDIR=obj

CC=g++
SRCDIR=src
SRC:= $(shell find $(SRCDIR)/ -name '*.cpp')
OBJ=$(addprefix $(OBJDIR)/, $(subst $(SRCDIR)/, ,$(SRC:.cpp=.o)))
IDIR=src/include
LIBS=-lsfml-graphics -lsfml-window -lsfml-system -I$(IDIR) -ljsoncpp

CFLAGS=-Wall -MP -MMD

$(BUILDDIR)/$(EXECUTABLE): $(OBJ)
	$(CC) $(OBJ) -o $@ $(CFLAGS) $(LIBS)

	cp -rf resources/maps/ $(BUILDDIR)
	cp -rf resources/sprites/ $(BUILDDIR)
	cp -rf resources/fonts/ $(BUILDDIR)

-include $(OBJ:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	mkdir -p $(dir $@)
	$(CC) -c $< -o $@ $(CFLAGS) $(LIBS)

debug: CFLAGS += -g
debug: $(BUILDDIR)/$(EXECUTABLE)
