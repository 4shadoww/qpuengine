#include <string>

#include "engine/component.hpp"
#include "engine/object.hpp"

component::component(std::string name){
  component::name = name;
}

component::~component(){}
void component::init(){}
void component::update(){}
void component::fixedUpdate(){}

void component::setHostObject_(object* ob){
  component::host = ob;
}

object* component::getHostObject(){
  return component::host;
}

std::string component::getName(){
  return component::name;
}

void component::setName(std::string name){
  component::name = name;
}
