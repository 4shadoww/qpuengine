#include <vector>
#include <string>
#include <algorithm>

#include <SFML/System/Vector2.hpp>

#include "engine/component/physicsbody.hpp"
#include "engine/globals.hpp"
#include "engine/math.hpp"

physicsbody::physicsbody(std::string name) : component(name){}

void physicsbody::init(){
  physicsbody::setType(physicsbody::t_convex);
}

std::vector<sf::Vector2f> &physicsbody::getVerticles(){
  return physicsbody::verticles;
}

void physicsbody::setVerticles(std::vector<sf::Vector2f> verticles){
  physicsbody::verticles = verticles;
}

void physicsbody::setVecticlePosition(int verticle, float x, float y){
  if(physicsbody::type == physicsbody::t_circle) return;
  physicsbody::verticles.at(verticle).x = x;
  physicsbody::verticles.at(verticle).y = y;
  physicsbody::updateMinMax();
  physicsbody::updateCenter();
}

void physicsbody::setVecticlePosition(int verticle, sf::Vector2f vector){
  if(physicsbody::type == physicsbody::t_circle) return;
  physicsbody::verticles.at(verticle) = vector;
  physicsbody::updateMinMax();
  physicsbody::updateCenter();
}

void physicsbody::removeVerticle(int i){
  if(physicsbody::type == physicsbody::t_circle) return;
  physicsbody::verticles.erase(physicsbody::verticles.begin() + i);
  physicsbody::updateMinMax();
  physicsbody::updateCenter();
}

unsigned int physicsbody::getVerticleCount(){
  return physicsbody::verticles.size();
}

void physicsbody::setVerticleCount(int count){
  if(physicsbody::type == physicsbody::t_circle) return;
  physicsbody::verticles.clear();

  for(int i = 0; i < count; i++){
    physicsbody::verticles.push_back(sf::Vector2f(0, 0));
  }
  physicsbody::updateMinMax();
  physicsbody::updateCenter();
}

sf::Vector2f physicsbody::getVerticlePosition(int verticle){
  sf::Vector2f pos = physicsbody::verticles.at(verticle) + physicsbody::host->getPosition();
  return pos;
}

sf::Vector2f physicsbody::getVerticleLocalPosition(int verticle){
  return physicsbody::verticles.at(verticle);
}

void physicsbody::updateCenter(){
    float minX;
    float maxX;
    float minY;
    float maxY;

    for(std::vector<sf::Vector2f>::iterator it = physicsbody::verticles.begin(); it != physicsbody::verticles.end(); ++it){
        if(it == physicsbody::verticles.begin()){
            minX = it->x;
            maxX = it->x;
            minY = it->y;
            maxY = it->y;
            continue;
        }
        if(it->x < minX) minX = it->x;
        if(it->x > maxX) maxX = it->x;
        if(it->y < minY) minY = it->y;
        if(it->y > maxY) maxY = it->y;
    }

    physicsbody::center = sf::Vector2f(minX + ((maxX - minX) / 2.f), minY + ((maxY - minY) / 2.f));
}

sf::Vector2f physicsbody::getCenter(){
  return (physicsbody::center + physicsbody::getPosition());
}

sf::Vector2f physicsbody::getLocalCenter(){
  return physicsbody::center;
}


sf::Vector2f physicsbody::getPosition(){
  return physicsbody::host->getPosition();
}

void physicsbody::setPosition(float x, float y){
  physicsbody::host->setPosition(x, y);
}

void physicsbody::setPosition(sf::Vector2f position){
  physicsbody::host->setPosition(position);
}

void physicsbody::move(float x, float y){
  physicsbody::host->move(x, y);
}

void physicsbody::move(sf::Vector2f position){
  physicsbody::host->move(position);
}

void physicsbody::setRadius(float radius){
  if(physicsbody::type != t_circle) return;
  physicsbody::radius = radius;
  physicsbody::size = 1.f;
  physicsbody::min = sf::Vector2f(-radius, -radius);
  physicsbody::max = sf::Vector2f(radius, radius);
}

float physicsbody::getRadius(){
  return physicsbody::radius;
}

// Set body type
void physicsbody::setType(bodytype type){
  switch(type){
    case physicsbody::t_convex:
      component::getHostObject()->removeChild("_debug_colliderDebugMesh");
      physicsbody::type = physicsbody::t_convex;
      physicsbody::verticles.clear();
      physicsbody::verticles.push_back(sf::Vector2f(-physicsbody::dVec * physicsbody::size, physicsbody::dVec * physicsbody::size));
      physicsbody::verticles.push_back(sf::Vector2f(physicsbody::dVec * physicsbody::size, physicsbody::dVec * physicsbody::size));
      physicsbody::verticles.push_back(sf::Vector2f(physicsbody::dVec * physicsbody::size, -physicsbody::dVec * physicsbody::size));
      physicsbody::verticles.push_back(sf::Vector2f(-physicsbody::dVec * physicsbody::size, -physicsbody::dVec * physicsbody::size));
      physicsbody::updateMinMax();
      physicsbody::updateCenter();
      break;
    case physicsbody::t_circle:
      component::getHostObject()->removeChild("_debug_colliderDebugMesh");
      physicsbody::type = physicsbody::t_circle;
      physicsbody::verticles.clear();
      physicsbody::setRadius(16.f);
      physicsbody::verticles.push_back(sf::Vector2f(0.f, 0.f));
      physicsbody::center = sf::Vector2f(0.f, 0.f);
      break;
  }
}

// Get body type
physicsbody::bodytype physicsbody::getType(){
  return physicsbody::type;
}

// Static
void physicsbody::setStatic(bool value){
  physicsbody::isstatic = value;
}

bool physicsbody::isStatic(){
  return physicsbody::isstatic;
}
// Trigger
void physicsbody::setTrigger(bool value){
  physicsbody::istrigger = value;
}
// Static
bool physicsbody::isTrigger(){
  return physicsbody::istrigger;
}

// Rotation
float physicsbody::getRotation(){
  return physicsbody::rotation;
}

bool physicsbody::setRotation(float angle){
  if(physicsbody::type == physicsbody::t_circle) return false;
  if(angle > 360.f || angle < -360.f) return false;

  float newangle = degToRad(physicsbody::rotation) - degToRad(angle);
  physicsbody::rotation = angle;

  sf::Vector2f origin = physicsbody::getLocalCenter();
  sf::Vector2f translated;

  for(std::vector<sf::Vector2f>::iterator it = physicsbody::verticles.begin(); it != physicsbody::verticles.end(); ++it){
    translated = (*it) - origin;
    it->x = translated.x * std::cos(newangle) - translated.y * std::sin(newangle);
    it->y = translated.x * std::sin(newangle) + translated.y * std::cos(newangle);
    (*it) += origin;
  }
  physicsbody::updateMinMax();
  physicsbody::updateCenter();
  return true;
}

bool physicsbody::rotate(float angle){
  if(angle > 360.f || angle < -360.f) return false;

  angle += physicsbody::rotation;
  if(angle > 360.f) angle -= 360.f;
  else if(angle < -360.f) angle += 360.f;

  return physicsbody::setRotation(angle);
}

void physicsbody::setSize(float size){
  if(physicsbody::size != 1.f){
    switch(physicsbody::type){
      case physicsbody::t_convex:
        for(std::vector<sf::Vector2f>::iterator it = physicsbody::verticles.begin(); it != physicsbody::verticles.end(); ++it){
          it->x /= physicsbody::size;
          it->y /= physicsbody::size;
        }
        break;
      case physicsbody::t_circle:
        physicsbody::radius /=  physicsbody::size;
        break;
    }
  }
  physicsbody::size = size;

  switch(physicsbody::type){
    case physicsbody::t_convex:
      for(std::vector<sf::Vector2f>::iterator it = physicsbody::verticles.begin(); it != physicsbody::verticles.end(); ++it){
        it->x *= physicsbody::size;
        it->y *= physicsbody::size;
      }
      break;
    case physicsbody::t_circle:
      physicsbody::radius *=  physicsbody::size;
      break;
  }
}

void physicsbody::setScale(float size){
  physicsbody::setSize(size);
}

void physicsbody::scale(float size){
  physicsbody::setSize(physicsbody::size + size);
}

float physicsbody::getSize(){
  return physicsbody::size;
}

float physicsbody::getScale(){
  return physicsbody::size;
}

// Colliding status
bool physicsbody::isColliding(){
  return physicsbody::iscolliding;
}
// Don't call this method
void physicsbody::setColliding_(bool status){
  physicsbody::iscolliding = status;
}
// Colliding list
void physicsbody::clearColliding_(){
  physicsbody::colliding.clear();
}
std::vector<object*> physicsbody::getColliding(){
  return physicsbody::colliding;
}
bool physicsbody::isColliding(object* ob){
  for(std::vector<object*>::iterator it = physicsbody::colliding.begin(); it != physicsbody::colliding.end(); ++it){
    if((*it) == ob) return true;
  }
  return false;
}
bool physicsbody::isColliding(std::string name){
  for(std::vector<object*>::iterator it = physicsbody::colliding.begin(); it != physicsbody::colliding.end(); ++it){
    if((*it)->getName() == name) return true;
  }
  return false;
}
bool physicsbody::isColliding(int id){
  for(std::vector<object*>::iterator it = physicsbody::colliding.begin(); it != physicsbody::colliding.end(); ++it){
    if((*it)->getId() == id) return true;
  }
  return false;
}

void physicsbody::updateMinMax(){
  float min_x, min_y;
  float max_x, max_y;

  for(std::vector<sf::Vector2f>::iterator it = physicsbody::verticles.begin(); it != physicsbody::verticles.end(); ++it){
    if(it == physicsbody::verticles.begin()){
      min_x = max_x = it->x;
      min_y = max_y = it->y;
      continue;
    }
    if(min_x > it->x)  min_x = it->x;
    if(max_x < it->x)  max_x = it->x;
    if(min_y > it->y)  min_y = it->y;
    if(max_y < it->y)  max_y = it->y;
  }
  physicsbody::min = sf::Vector2f(min_x, min_y);
  physicsbody::max = sf::Vector2f(max_x, max_y);
}

sf::Vector2f physicsbody::getMin(){
  return physicsbody::min;
}

sf::Vector2f physicsbody::getMax(){
  return physicsbody::max;
}

float physicsbody::getVelocity(){
  if(!physicsbody::velocity_is_valid){
    physicsbody::last_velocity = physicsbody::velocity;
    physicsbody::velocity = qpumath::getDistance(host->getPreviousLastPosition(), host->getLastPosition()) / engine->fixedDeltaTime();
    physicsbody::velocity_is_valid = true;
  }
  return physicsbody::velocity;
}

void physicsbody::setVelocityInvalid_(){
  physicsbody::velocity_is_valid = false;
}

void physicsbody::updatePhyenPosition_(){
  physicsbody::phyen_pos = host->getPosition();
}

float physicsbody::getPhyenVelocity_(){
  if(physicsbody::use_last_position){
    physicsbody::use_last_position = false;
    return qpumath::getDistance(host->getPreviousLastPosition(), host->getLastPosition());
  }
  return qpumath::getDistance(host->getPosition(), physicsbody::phyen_pos);
}

void physicsbody::useLastPosition_(bool value){
  physicsbody::use_last_position = value;
}

float physicsbody::getLastVelocity(){
  return physicsbody::last_velocity;
}

float physicsbody::getAcceleration(){
  float acceleration = physicsbody::getVelocity();
  return (acceleration - last_velocity) ;
}

void physicsbody::setMass(float mass){
  physicsbody::mass = mass;
}

float physicsbody::getMass(){
  return physicsbody::mass;
}
