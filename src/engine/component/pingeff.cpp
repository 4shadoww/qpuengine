#include "engine/component/pingeff.hpp"

#include "engine/globals.hpp"

pingeff::pingeff() : component("pingeff"){}

pingeff::pingeff(std::string name) : component(name){}

void pingeff::init(){
  if(!host->meshtype == object::t_circle){
    host->createCircleMesh();
  }
  host->circlemesh->setFillColor(sf::Color::Transparent);
  host->circlemesh->setOutlineColor(sf::Color::Blue);
  host->circlemesh->setRadius(0.f);
  host->circlemesh->setOutlineThickness(-1.f);
}

void pingeff::setDestroyAfter(bool value){
  pingeff::destroy_after = value;
}

bool pingeff::getDestroyAfter(){
  return pingeff::destroy_after;
}

void pingeff::setTime(float value){
  pingeff::m_time = value;
}

float pingeff::getTime(){
  return pingeff::m_time;
}

void pingeff::setMaxRadius(float value){
  pingeff::max_radius = value;
}

float pingeff::getMaxRadius(){
  return pingeff::max_radius;
}

void pingeff::update(){
  if(!pingeff::active){
    return;
  }
  if(pingeff::spent_time >= pingeff::m_time){
    if(pingeff::destroy_after) host->destroy();
    pingeff::active = false;
    return;
  }
  float per = pingeff::m_time / pingeff::spent_time;
  float new_radius = max_radius * per;
  host->circlemesh->setRadius(new_radius);
  host->circlemesh->setOrigin(new_radius, new_radius);
  pingeff::spent_time += engine->deltaTime();
}
