#include "engine/component/raycast.hpp"
#include "engine/component/physicsbody.hpp"
#include "engine/globals.hpp"
#include "engine/math.hpp"
#include "engine/customvertex/line.hpp"

raycast::raycast(std::string name) : component(name){}
raycast::raycast() : component("raycast"){}
raycast::~raycast(){
  raycast::lineob->destroy();
}

float raycast::getAngle(){
  return raycast::angle;
}

float raycast::getMagnitude(){
  return raycast::magnitude;
}

void raycast::updateRay_(float distance, sf::Vector2f position, physicsbody* collider){
  raycast::distance = distance;
  raycast::position = position;
  raycast::collider = collider;
}

physicsbody* raycast::getHittingBody(){
  return raycast::collider;
}

bool raycast::isColliding(){
  return raycast::collider;
}

float raycast::getHitDistance(){
  return raycast::distance;
}

sf::Vector2f raycast::getHitPosition(){
  return raycast::position;
}

bool raycast::setAngle(float angle){
  if(angle <= 360.f && angle >= -360.f){
    if(angle < 0.f) angle += 360.f;
    raycast::angle = angle;
    return true;
  }
  return false;
}

bool raycast::rotate(float angle){
  if(angle <= 360.f && angle >= -360.f){
    raycast::angle += angle;
    if(raycast::angle > 360.f) raycast::angle -= 360.f;
    else if(raycast::angle < -360.f) raycast::angle += 360;
    return true;
  }
  return false;
}

void raycast::setMagnitude(float magnitude){
  raycast::magnitude = magnitude;
}

sf::Vector2f raycast::getOrigin(){
  return host->getPosition();
}

sf::Vector2f raycast::getEnd(){
  return qpumath::getEndPoint(host->getPosition(), angle, magnitude);
}

void raycast::setDebug(bool value){
  if(value == raycast::debug) return;
  raycast::debug = value;

  if(raycast::debug){
    raycast::lineob = engine->createObject("_debug_raycast");
    //raycast::lineob->setAlwaysLast(true);
    raycast::line = lineob->setCustomMesh(new linevertex);
    raycast::line->setColor(raycast::dcolor);
  }else{
    raycast::lineob->destroy();
  }
}

void raycast::setDebugColor(sf::Color color){
  raycast::dcolor = color;
  if(raycast::debug){
    raycast::line->setColor(color);
  }
}

void raycast::updateRaycast(){
  engine->phyen->updateRaycast(this);
}

void raycast::update(){
  if(raycast::debug){
    raycast::line->setPositions(host->getPosition(), raycast::getEnd());
  }
}
