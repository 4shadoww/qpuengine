#include "engine/custommesh.hpp"
#include "engine/object.hpp"

void custommesh::draw(sf::RenderTarget& target, sf::RenderStates states) const{
  states.transform *= getTransform();
  states.texture = &m_texture;
  target.draw(m_vertices, states);
}

void custommesh::setHostObject_(object* host){
  custommesh::host = host;
}

void custommesh::create(){}
void custommesh::update(){}
