#include "engine/customvertex/line.hpp"

linevertex::linevertex(){
  m_vertices.setPrimitiveType(sf::Lines);
  m_vertices.resize(2);
  m_vertices[0].color = sf::Color::Green;
  m_vertices[1].color = sf::Color::Green;
}

void linevertex::setPositions(sf::Vector2f first, sf::Vector2f second){
  m_vertices[0].position = first;
  m_vertices[1].position = second;
}

void linevertex::setPositions(float x0, float y0, float x1, float y1){
  linevertex::setPositions(sf::Vector2f(x0, y0), sf::Vector2f(x1, y1));
}

void linevertex::setFirstPosition(sf::Vector2f pos){
  m_vertices[0].position = pos;
}

void linevertex::setFirstPosition(float x, float y){
  linevertex::setFirstPosition(sf::Vector2f(x, y));
}

void linevertex::setSecondPosition(sf::Vector2f pos){
  m_vertices[1].position = pos;
}

void linevertex::setSecondPosition(float x, float y){
  linevertex::setSecondPosition(sf::Vector2f(x, y));
}

void linevertex::setColor(sf::Color color){
  m_vertices[0].color = color;
  m_vertices[1].color = color;
}
