#include "engine/customvertex/simple_particles.hpp"
#include "engine/globals.hpp"
#include "engine/math.hpp"

void simple_particles::setAngles(float min_angle, float max_angle){
  simple_particles::min_angle = min_angle;
  simple_particles::max_angle = max_angle;
  simple_particles::update_angles = true;
}

void simple_particles::setParticleCount(unsigned int count){
  simple_particles::quad_count = count;
}

void simple_particles::setSpawnRadius(float radius){
  simple_particles::radius = radius;
}

void simple_particles::setLifeTimes(float min, float max){
  simple_particles::point_min_life_time = min;
  simple_particles::point_max_life_time = max;
}

void simple_particles::setMinLifeTime(float time){
  simple_particles::point_min_life_time = time;
}

void simple_particles::setMaxLifeTime(float time){
  simple_particles::point_min_life_time = time;
}

void simple_particles::setColors(sf::Color start, sf::Color end){
  simple_particles::start_color =  start;
  simple_particles::end_color = end;
}

void simple_particles::setStartColor(sf::Color color){
  simple_particles::start_color = color;
}

void simple_particles::setEndColor(sf::Color color){
  simple_particles::end_color = color;
}

void simple_particles::setSize(float size){
  simple_particles::point_size = size;
}

void simple_particles::setVelocity(float velocity){
  simple_particles::velocity_x = velocity;
  simple_particles::velocity_y = velocity;
}

void simple_particles::setVelocityX(float velocity){
  simple_particles::velocity_x = velocity;
}

void simple_particles::setVelocityY(float velocity){
  simple_particles::velocity_y = velocity;
}

void simple_particles::useGlobalSpace(bool value){
  simple_particles::global_space = value;
}

float simple_particles::newLifeTime(){
  return point_min_life_time + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(point_max_life_time-point_min_life_time)));
}

void simple_particles::calculateStep(simple_particles::particle& par){
  par.color_r_step = color_r_delta / (par.max_life_time * engine->re->getFramerate());
  par.color_g_step =  color_g_delta / (par.max_life_time * engine->re->getFramerate());
  par.color_b_step = color_b_delta / (par.max_life_time * engine->re->getFramerate());
}

void simple_particles::addParticles(unsigned int count){
  float angle;
  sf::Vector2f direction;
  particle par;
  for(unsigned int i = 0; i < count; i++){
    // Position
    float pos_x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/radius * 2));
    float pos_y = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/radius * 2));

    // Velocity
    angle = min_angle + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max_angle-min_angle)));
    direction = qpumath::angleToVector(angle);

    // Values to particle
    if(global_space){
      par.position = sf::Vector2f(pos_x, pos_y) + host->getPosition();
    }else{
      par.position = sf::Vector2f(pos_x, pos_y);
    }
    par.velocity = sf::Vector2f(velocity_x * direction.x, velocity_y * direction.y);
    par.max_life_time = simple_particles::newLifeTime();
    simple_particles::resetColor(par);
    simple_particles::calculateStep(par);
    simple_particles::particles.push_back(par);
  }
}

void simple_particles::initPositions(){
  simple_particles::particles.clear();
  // Calculate color delta
  color_r_delta = end_color.r - start_color.r;
  color_g_delta = end_color.g - start_color.g;
  color_b_delta = end_color.b - start_color.b;
  // Create particles
  simple_particles::addParticles(quad_count);
}

simple_particles::particle &simple_particles::getPos(unsigned int ind){
  return simple_particles::particles.at(ind);
}

void simple_particles::resetColor(simple_particles::particle& par){
  par.current_color = start_color;
  par.color_r = start_color.r;
  par.color_g = start_color.g;
  par.color_b = start_color.b;
}

void simple_particles::updateColor(simple_particles::particle& par){
  par.color_r += par.color_r_step;
  par.color_g += par.color_g_step;
  par.color_b += par.color_b_step;

  par.current_color.r = par.color_r;
  par.current_color.g = par.color_g;
  par.current_color.b = par.color_b;
}

void simple_particles::calculatePos(simple_particles::particle& par){
  if(par.dead) return;

  par.life_time += engine->deltaTime();

  if(par.life_time > par.max_life_time){
    // Check should particle die
    if(particles.size() - deadparticles > quad_count){
      par.dead = true;
      deadparticles++;
      return;
    }
    // Get random pos
    float pos_x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/radius * 2));
    float pos_y = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/radius * 2));
    if(global_space){
      par.position = host->getPosition();
      par.position.x += pos_x;
      par.position.y += pos_y;
    }else{
      par.position.x = pos_x;
      par.position.y = pos_y;
    }
    par.life_time = 0.f;
    simple_particles::resetColor(par);
    par.max_life_time = simple_particles::newLifeTime();
    simple_particles::calculateStep(par);
    if(update_angles){
      float angle = min_angle + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max_angle-min_angle)));
      sf::Vector2f direction = qpumath::angleToVector(angle);
      par.velocity = sf::Vector2f(velocity_x * direction.x, velocity_y * direction.y);
      update_angles = false;
    }
  }else{
    par.position.x += par.velocity.x * engine->deltaTime() * 10;
    par.position.y += par.velocity.y * engine->deltaTime() * 10;
    simple_particles::updateColor(par);
  }
}

void simple_particles::create(){
  sf::Vector2f pos;
  simple_particles::half_point = simple_particles::point_size / 2;

  simple_particles::initPositions();

  m_vertices.setPrimitiveType(sf::Quads);
  m_vertices.resize(quad_count * 4);

  created = true;
}

void simple_particles::updateParticleCount(){
  // Remove dead verticles
  if(m_vertices.getVertexCount() / 4 > quad_count){
    bool deleted = false;
    for(std::vector<particle>::iterator it = particles.begin(); it != particles.end();){
      if(it->dead){
        particles.erase(it);
        deleted = true;
        deadparticles--;
        continue;
      }
      ++it;
    }
    if(deleted) m_vertices.resize(particles.size() * 4);
  // Add more particles
  }else if(m_vertices.getVertexCount() / 4 < quad_count){
    float diff = quad_count - particles.size();
    m_vertices.resize(quad_count * 4);
    simple_particles::addParticles(diff);
  }
}

void simple_particles::update(){
  if(!created) return;

  sf::Vector2f pos;
  if(global_space) pos = host->getPosition();
  for(std::pair<unsigned int, std::vector<particle>::iterator> it(0, particles.begin()); it.first < particles.size() && it.second != particles.end(); it.first++, ++it.second){
    particle& par = *it.second;
    sf::Vertex* point = &m_vertices[it.first * 4];
    simple_particles::calculatePos(par);
    // Update position
    if(global_space){
      point[0].position = sf::Vector2f(half_point + (par.position.x - pos.x), half_point + (par.position.y - pos.y));
      point[1].position = sf::Vector2f(-half_point + (par.position.x - pos.x), half_point + (par.position.y - pos.y));
      point[2].position = sf::Vector2f(-half_point +(par.position.x - pos.x), -half_point + (par.position.y - pos.y));
      point[3].position = sf::Vector2f(half_point + (par.position.x - pos.x), -half_point + (par.position.y - pos.y));
    }else{
      point[0].position = sf::Vector2f(half_point + par.position.x, half_point + par.position.y);
      point[1].position = sf::Vector2f(-half_point + par.position.x, half_point + par.position.y);
      point[2].position = sf::Vector2f(-half_point + par.position.x, -half_point + par.position.y);
      point[3].position = sf::Vector2f(half_point + par.position.x, -half_point + par.position.y);
    }
    // Update color
    point[0].color = par.current_color;
    point[1].color = par.current_color;
    point[2].color = par.current_color;
    point[3].color = par.current_color;
  }
  simple_particles::updateParticleCount();
}
