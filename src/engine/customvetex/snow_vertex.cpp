#include "engine/customvertex/snow_vertex.hpp"
#include "engine/globals.hpp"

// TODO Rewrite to be like minimal version of simple_particles

void snow_vertex::initPositions(){
  snow_vertex::particles.clear();
  for(unsigned int i = 0; i < quad_count; i++){
    particle par;
    // Position
    float pos_x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/x_spawn));
    float pos_y = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/y_spawn));

    // Velocity
    float lowest = -(velocity_x / 2);
    float randX = lowest + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(velocity_x-lowest)));

    par.position = sf::Vector2f(pos_x, pos_y);
    par.velocity = sf::Vector2f(randX, velocity_y);
    snow_vertex::particles.push_back(par);
  }
}

void snow_vertex::create(){
  snow_vertex::width = engine->wm->getWidth();
  snow_vertex::height = engine->wm->getHeight();
  snow_vertex::x_spawn = snow_vertex::width;
  snow_vertex::y_spawn = snow_vertex::height;

  snow_vertex::half_point = snow_vertex::point_size / 2;

  snow_vertex::initPositions();

  m_vertices.setPrimitiveType(sf::Quads);
  m_vertices.resize(quad_count * 4);

  for(unsigned int i = 0; i < quad_count; i++){
    sf::Vertex* point = &m_vertices[i * 4];

    point[0].color = snow_vertex::color;
    point[1].color = snow_vertex::color;
    point[2].color = snow_vertex::color;
    point[3].color = snow_vertex::color;
  }
  created = true;
}

void snow_vertex::calculatePos(particle& par){
  par.position.x = par.position.x + par.velocity.x * engine->deltaTime() * 10;
  par.position.y = par.position.y + par.velocity.y * engine->deltaTime() * 10;
  if(par.position.x - half_point > snow_vertex::width || par.position.y - half_point > snow_vertex::height){
    float x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/x_spawn));
    par.position = sf::Vector2f(x, -half_point);
  }
}

void snow_vertex::update(){
  if(!created) return;
  for(std::pair<unsigned int, std::vector<particle>::iterator> it(0, particles.begin()); it.first < particles.size() && it.second != particles.end(); it.first++, ++it.second){
    sf::Vertex* point = &m_vertices[it.first * 4];
    particle& par = *it.second;
    snow_vertex::calculatePos(par);
    point[0].position = sf::Vector2f(half_point + par.position.x, half_point + par.position.y);
    point[1].position = sf::Vector2f(-half_point + par.position.x, half_point + par.position.y);
    point[2].position = sf::Vector2f(-half_point + par.position.x, -half_point + par.position.y);
    point[3].position = sf::Vector2f(half_point + par.position.x, -half_point + par.position.y);
  }
}
