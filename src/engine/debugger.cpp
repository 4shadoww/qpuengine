#include <chrono>
#include <thread>

#include <SFML/Window/Keyboard.hpp>

#include "engine/debugger.hpp"
#include "engine/globals.hpp"
#include "engine/component/pingeff.hpp"

void debugger::update(){
  debugger::updateShowStats();
  debugger::updateDebugDraw();
}

void debugger::updateShowStats(){
  if(engine->hasFocus() && engine->keyPressed("show_stats") && debugger::f1released){
    debugger::show_stats = !debugger::show_stats;
    debugger::showStats(debugger::show_stats);
    debugger::f1released = false;
  }else if(!engine->keyPressed("show_stats")){
    debugger::f1released = true;
  }
  if(debugger::show_stats) debugger::statsUpdate();
}

void debugger::showStats(bool enable){
  if(enable){
    debugger::stats = engine->objmgr->createObject("debug_stats");
    debugger::stats->setView(1);
    debugger::stats->createTextMesh();
    debugger::stats->textmesh->setFont(*(engine->fontmgr->getFont("DejaVu Sans")));
    debugger::stats->textmesh->setString("debug_stats");
    debugger::stats->textmesh->setScale(0.5, 0.5);
  }else{
    debugger::stats->destroy();
    debugger::stats = nullptr;
  }
}

void debugger::statsUpdate(){
  std::string stats = "";
  float smoothing = 0.9f;
  float fps = 1 / engine->deltaTime();
  fps = (last_fps * smoothing) + (fps * (1.0-smoothing));
  last_fps = fps;
  // Fps
  stats += "fps: "+std::to_string(fps)+"\n";
  // Object count
  stats += "objects: "+std::to_string(engine->objmgr->getObjectCount());
  debugger::stats->textmesh->setString(stats);

}

// Debug draw
void debugger::updateDebugDraw(){
  if(engine->hasFocus() && engine->keyPressed("debug_draw") && debugger::f2released){
    if(!engine->phyen->debugDrawEnabled()){
      engine->phyen->enableDebugDraw();
    }else{
      engine->phyen->disableDebugDraw();
    }
    debugger::f2released = false;
  }else if(!engine->keyPressed("debug_draw")){
    debugger::f2released = true;
  }
}

// Ping object
void debugger::pingObject(object* ob){
  object* ping = engine->createObject("pingpong");
  ping->createCircleMesh();
  pingeff* eff = ping->addComponent(new pingeff);
  eff->setDestroyAfter(true);
  ping->setPosition(ob->getPosition());
}

void debugger::pingPosition(sf::Vector2f pos){
  object* ping = engine->createObject("pingpong");
  ping->createCircleMesh();
  pingeff* eff = ping->addComponent(new pingeff);
  eff->setDestroyAfter(true);
  ping->setPosition(pos);
}

object* debugger::debugMark(float x, float y, float radius){
  object* ob = engine->createObject("debugMark_");
  ob->createCircleMesh();
  ob->circlemesh->setRadius(radius);
  ob->circlemesh->setOrigin(radius, radius);
  ob->circlemesh->setFillColor(sf::Color::Red);
  ob->setPosition(x, y);
  return ob;
}

void debugger::freeze(int time){
  std::this_thread::sleep_for(std::chrono::milliseconds(time));
}
