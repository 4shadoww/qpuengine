#include "engine/entitymanager.hpp"
#include "engine/entity.hpp"

#include "entity/testentity.hpp"
#include "entity/error.hpp"

entitymanager::entitymanager(){
  entity* boot = new testentity;
  entitymanager::currententity = boot;
}

void entitymanager::initEntity(){
  if(!entitymanager::initialized){
    entitymanager::currententity->init();
    entitymanager::initialized = true;
  }
}

void entitymanager::loadEntity(entity* newentity){
  delete entitymanager::currententity;
  entitymanager::initialized = false;
  entitymanager::currententity = newentity;
}

void entitymanager::update(){
  entitymanager::currententity->update();
}

void entitymanager::fixedUpdate(){
  entitymanager::currententity->fixedUpdate();
}
