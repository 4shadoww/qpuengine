#include <string>
#include <SFML/Graphics/Font.hpp>

#include "engine/fontloader.hpp"
#include "engine/globals.hpp"

void fontloader::loadFont(std::string path){
  sf::Font* font = new sf::Font;
  font->loadFromFile(engine->getPath()+path);
  fontloader::fonts.push_back(font);
}

sf::Font* fontloader::getFont(std::string name){
  for(std::vector<sf::Font*>::iterator it = fontloader::fonts.begin(); it != fontloader::fonts.end(); ++it){
    if((*it)->getInfo().family == name) return (*it);
  }
  return nullptr;
}
