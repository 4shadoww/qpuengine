#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Mouse.hpp>

#include "engine/input_manager.hpp"
#include "engine/globals.hpp"

input_manager::input_manager(){
  input_manager::loadDefaults();
}

void input_manager::clearLists(){
  input_manager::key_bindings.clear();
}

void input_manager::loadDefaults(){
  input_manager::clearLists();
  // Key bindings
  input_manager::bindKey(sf::Keyboard::F1, "show_stats");
  input_manager::bindKey(sf::Keyboard::F2, "debug_draw");
  input_manager::bindKey(sf::Keyboard::W, "mov_up");
  input_manager::bindKey(sf::Keyboard::A, "mov_left");
  input_manager::bindKey(sf::Keyboard::S, "mov_down");
  input_manager::bindKey(sf::Keyboard::D, "mov_right");

}

bool input_manager::bindKey(sf::Keyboard::Key key, std::string name){
  for(std::unordered_map<std::string, eveK>::iterator it = key_bindings.begin(); it != key_bindings.end(); ++it){
    if(it->first == name && it->second.key == key) return false;
  }
  std::pair<std::string, eveK> pair(name, eveK(key));
  key_bindings.insert(pair);
  return true;
}

bool input_manager::keyPressed(sf::Keyboard::Key key, bool req_focus){
  if(req_focus && !engine->hasFocus()) return false;
  return sf::Keyboard::isKeyPressed(key);
}

bool input_manager::keyPressed(std::string name, bool req_focus){
  if(req_focus && !engine->hasFocus()) return false;
  for(std::unordered_map<std::string, eveK>::iterator it = key_bindings.begin(); it != key_bindings.end(); ++it){
    if(it->first == name && sf::Keyboard::isKeyPressed(it->second.key)) return true;
  }
  return false;
}

bool input_manager::isBinded(sf::Keyboard::Key key){
  for(std::unordered_map<std::string, eveK>::iterator it = key_bindings.begin(); it != key_bindings.end(); ++it){
    if(it->second.key == key) return true;
  }
  return false;
}

bool input_manager::isBinded(std::string name){
  for(std::unordered_map<std::string, eveK>::iterator it = key_bindings.begin(); it != key_bindings.end(); ++it){
    if(it->first == name) return true;
  }
  return false;
}

bool input_manager::isBinded(sf::Keyboard::Key key, std::string name){
  for(std::unordered_map<std::string, eveK>::iterator it = key_bindings.begin(); it != key_bindings.end(); ++it){
    if(it->first == name && it->second.key == key) return true;
  }
  return false;
}

sf::Vector2i input_manager::getMouseRawPos(){
  return sf::Mouse::getPosition();
}

sf::Vector2i input_manager::getMousePos(){
  return input_manager::getMousePos(engine->wm->sfwindow);
}

sf::Vector2i input_manager::getMousePos(sf::RenderWindow* window){
  return sf::Mouse::getPosition(*window);
}

sf::Vector2f input_manager::getMouseGlobalPos(){
  return input_manager::getMouseGlobalPos(engine->wm->sfwindow, engine->re->getCamPosition());
}

sf::Vector2f input_manager::getMouseGlobalPos(sf::Vector2f relative){
  return input_manager::getMouseGlobalPos(engine->wm->sfwindow, relative);
}

sf::Vector2f input_manager::getMouseGlobalPos(sf::RenderWindow* window){
  return input_manager::getMouseGlobalPos(window, engine->re->getCamPosition());
}

sf::Vector2f input_manager::getMouseGlobalPos(sf::RenderWindow* window, sf::Vector2f relative){
  return  relative - engine->wm->sfwindow->mapPixelToCoords(input_manager::getMousePos(window));
}
