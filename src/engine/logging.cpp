#include "engine/logging.hpp"

logger::logger(){
  logger::name = "default";
}

logger::logger(std::string name){
  logger::name = name;
}

std::string logger::getName(){
  return logger::name;
}

void logger::stdout(){
  std::cout << std::endl;
}

void logger::stderr(){
  std::cerr << std::endl;
}

logger* logging::create(std::string name){
  logger* lo = new logger(name);
  logging::loggers.push_back(lo);
  return lo;
}

logger* logging::get(std::string name){
  for(std::vector<logger*>::iterator it = logging::loggers.begin(); it != logging::loggers.end(); ++it){
    if(name == (*it)->getName()) return (*it);
  }
  return nullptr;
}

bool logging::destroy(std::string name){
  for(std::vector<logger*>::iterator it = logging::loggers.begin(); it != logging::loggers.end(); ++it){
    if(name == (*it)->getName()){
      delete (*it);
      logging::loggers.erase(it);
    }
  }
  return false;
}
