#include "engine/math.hpp"
#include <cmath>

namespace qpumath{

float smoothing(float current, float last, float smoothing){
  return (last * smoothing) + (current * (1.0-smoothing));
}

float getAngle(sf::Vector2f delta){
  float angle = std::atan2(delta.y, delta.x) * 180 / PI;
  if(angle < 0) angle += 360.f;
  if(angle > 360.f) angle -= 360.f;
  return angle;
  }

sf::Vector2f angleToVector(float angle){
  angle = degToRad(angle);
  return sf::Vector2f(std::cos(angle), std::sin(angle));
}

sf::Vector2f getEndPoint(sf::Vector2f start, float angle, float magnitude){
  sf::Vector2f vec;
  vec.x = start.x + magnitude * std::cos((PI * angle) / 180);
  vec.y = start.y + magnitude * std::sin((PI * angle) / 180);
  return vec;
}

sf::Vector2f getCircleAndLineIntersection(sf::Vector2f a, sf::Vector2f b, float angle, float magnitude, sf::Vector2f c, float radius){
  if(angle < 0) angle += 360;
  float corner_angle = getAngle(c - a);
  if(angle > corner_angle) corner_angle = angle - corner_angle;
  else corner_angle = corner_angle - angle;
  float acdist = getDistance(a, c);
  float length0 = std::cos(degToRad(corner_angle)) * acdist;
  if(length0 > magnitude + radius || length0 < -magnitude - radius || (acdist > radius && length0 < 0.f)) return sf::Vector2f(FLT_MAX, FLT_MAX);
  sf::Vector2f point = getEndPoint(a, angle, length0);
  float distcenter = getDistance(c, point);
  float legth1 = std::sqrt(radius * radius - distcenter * distcenter);
  if(std::isnan(legth1)) return sf::Vector2f(FLT_MAX, FLT_MAX);
  if(acdist > radius) legth1 = -legth1;
  return getEndPoint(point, angle, legth1);
}

sf::Vector2f getCircleAndLineIntersection(sf::Vector2f a, float angle, float magnitude, sf::Vector2f c, float radius){
  return getCircleAndLineIntersection(a, getEndPoint(a, angle, magnitude), angle, magnitude, c, radius);
}

sf::Vector2f getLinesIntersection(sf::Vector2f a, sf::Vector2f b, sf::Vector2f c, sf::Vector2f d){
  sf::Vector2f r = b - a;
  sf::Vector2f s = d - c;
  float f = r.x * s.y - r.y * s.x;
  float u = ((c.x - a.x) * r.y - (c.y - a.y) * r.x) / f;
  float t = ((c.x - a.x) * s.y - (c.y - a.y) * s.x) / f;
  if(0 <= u && u <= 1 && 0 <= t && t <= 1){
    return sf::Vector2f(a.x + t * r.x, a.y + t * r.y);
  }
  return sf::Vector2f(FLT_MAX, FLT_MAX);
}

float getDistance(sf::Vector2f v0, sf::Vector2f v1){
  v0 = v0 - v1;
  return std::sqrt(v0.x * v0.x + v0.y * v0.y);
}

float dotproduct(sf::Vector2f v0, sf::Vector2f v1){
  return v0.x * v1.x + v0.y * v1.y;
}

sf::Vector2f getAxis(sf::Vector2f vec0, sf::Vector2f vec1){
  sf::Vector2f axis;
  float len_axis;

  axis = vec0 - vec1;
  len_axis = std::sqrt(axis.x * axis.x + axis.y * axis.y);
  axis.x /= len_axis;
  axis.y /= len_axis;
  return sf::Vector2f(-axis.y, axis.x);
}

bool inSector(float angle, float start, float end){
  return (start<=end) ? (angle>=start && angle<=end) : (angle>=start || angle<=end);
}

float invertAngle(float angle){
  angle = angle + 180.f;
  if(angle < 0) angle += 360.f;
  if(angle > 360.f) angle -= 360.f;

  return angle;
}

float fixedAngle(float angle){
  if(angle < 0.f) angle += 360.f;
  if(angle > 360.f) angle -= 360.f;
  return angle;
}

float semiFixedAngle(float angle){
  if(angle < -360.f) angle += 360.f;
  if(angle > 360.f) angle -= 360.f;
  return angle;
}

float anglesDiff(float angle0, float angle1){
  return (180.f - std::abs(std::abs(angle0 - angle1) - 180.f));
}

float getMinAngleDiff(float angle0, float angle1){
  float angle = anglesDiff(angle0, angle1);
  if(angle > 180.f) angle = 360.f - angle;
  return angle;
}

};
