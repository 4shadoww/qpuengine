#include <cmath>

#include "engine/globals.hpp"
#include "engine/object.hpp"
#include "engine/component/physicsbody.hpp"
#include "engine/component.hpp"
#include "engine/custommesh.hpp"
#include "engine/math.hpp"

// Constructor
object::object(std::string name, int loop){
  object::name = name;
  object::loop = loop;
  object::meshtype = t_none;
}
// Destructor
object::~object(){
  object::clearComponents();
  delete object::circlemesh;
  delete object::convexmesh;
  delete object::rectanglemesh;
  delete object::sprite;
  delete object::textmesh;
  delete object::cumesh;
}

// Object update
void object::update(){
  for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end(); ++it){
    (*it)->update();
  }
}

// Object fixed update
void object::fixedUpdate(){
  // Set last position
  object::updateLastPosition_();

  for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end(); ++it){
    (*it)->fixedUpdate();
  }
}

// Create circle mesh
void object::createCircleMesh(){
  object::destroyMesh();
  object::meshtype = t_circle;
  object::circlemesh = new sf::CircleShape(100.f);
  object::circlemesh->setOrigin(object::circlemesh->getRadius(), object::circlemesh->getRadius());
  object::updateMeshPosition();
}
// Create convex mesh
void object::createConvexMesh(){
  object::destroyMesh();
  object::meshtype = t_convex;
  object::convexmesh = new sf::ConvexShape(4);
  object::convexmesh->setPoint(0, sf::Vector2f(16, 16));
  object::convexmesh->setPoint(1, sf::Vector2f(-16, 16));
  object::convexmesh->setPoint(2, sf::Vector2f(-16, -16));
  object::convexmesh->setPoint(3, sf::Vector2f(16, -16));
  object::updateMeshPosition();
}
// Create rectangle mesh
void object::createRectangleMesh(){
  object::destroyMesh();
  object::meshtype = t_rectangle;
  object::rectanglemesh = new sf::RectangleShape;
  object::updateMeshPosition();
}
void object::createSprite(){
  object::destroyMesh();
  object::meshtype = t_sprite;
  object::sprite = new sf::Sprite;
  object::updateMeshPosition();
}

// Create text mesh
void object::createTextMesh(){
  object::destroyMesh();
  object::meshtype = t_text;
  object::textmesh = new sf::Text();
  object::updateMeshPosition();
}
// Check has object nesh
bool object::hasMesh(){
  if(object::circlemesh || object::convexmesh || object::rectanglemesh || object::sprite || object::textmesh || object::cumesh)
    return true;

  return false;
}
// Destroy mesh
void object::destroyMesh(){
  object::meshtype = t_none;
  delete object::circlemesh;
  object::circlemesh = nullptr;
  delete object::convexmesh;
  object::convexmesh = nullptr;
  delete object::rectanglemesh;
  object::rectanglemesh = nullptr;
  delete object::sprite;
  object::sprite = nullptr;
  delete object::textmesh;
  object::textmesh = nullptr;
  delete object::cumesh;
  object::cumesh = nullptr;
}

// Destroy object
void object::destroy(){
  // If has parent remove this from it's childs
  if(object::hasParent()){
    object::removeParent();
  }

  // Delete childs
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end();){
    (*it)->destroy();
  }
  object::markedfordeletion = true;
}
// Check is this object about to be destroyed
bool object::destroyThis(){
  return object::markedfordeletion;
}

// Check is this object about to be destroyed
bool object::destroyed(){
  return object::markedfordeletion;
}

// Get id
int object::getId(){
  return object::id;
}

std::string object::getName(){
  return object::name;
}

// Set id
/* Note: don't call this directly
   This should be called only using the object manager
   Otherwise it could cause crash */
void object::setId_(int id){
  object::id = id;
}

void object::setName(std::string name){
  object::name = name;
}

sf::Vector2f object::getPosition(){
  return object::position;
}

sf::Vector2f object::getLastPosition(){
  return object::last_position;
}

sf::Vector2f object::getPreviousLastPosition(){
  return object::before_last_position;
}

void object::updateLastPosition_(){
  object::before_last_position = object::last_position;
  object::last_position = object::position;
}

void object::setPosition(float x, float y){
  // Calculate diff
  float xdiff = x - object::position.x;
  float ydiff = y - object::position.y;

  object::position.x = x;
  object::position.y = y;
  // Update mesh position
  object::updateMeshPosition();
  // Set childs position
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    (*it)->setPosition((*it)->getPosition().x + xdiff, (*it)->getPosition().y + ydiff);
  }
}

void object::setPosition(sf::Vector2f position){
  object::setPosition(position.x, position.y);
}

void object::move(float x, float y){
  // Move object
  object::position.x += x;
  object::position.y += y;
  // Update mesh position
  object::updateMeshPosition();
  // Move childs position
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    (*it)->move(x, y);
  }
}

void object::move(sf::Vector2f position){
  object::move(position.x, position.y);
}

void object::updateMeshPosition(){
  switch(object::meshtype){
    case t_none : break;
    case t_circle :
      object::circlemesh->setPosition(object::position.x, object::position.y);
      break;
    case t_convex :
      object::convexmesh->setPosition(object::position.x, object::position.y);
      break;
    case t_rectangle :
      object::rectanglemesh->setPosition(object::position.x, object::position.y);
      break;
    case t_sprite :
      object::sprite->setPosition(object::position.x, object::position.y);
      break;
    case t_text :
      object::textmesh->setPosition(object::position.x, object::position.y);
      break;
    case t_custom :
      object::cumesh->setPosition(object::position.x, object::position.y);
      break;
  }
}

bool object::setRotation(float angle){
  if(angle > 360.f || angle < -360.f) return false;

  float diff = angle - object::rotation;
  if(diff > 360.f) diff -= 360.f;
  if(diff < -360.f) diff += 360.f;

  if(object::hasCollider()){
    object::collider->rotate(-diff);
  }
  if(object::hasMesh()){
    switch(object::meshtype){
      case t_none : break;
      case object::t_circle :
        object::circlemesh->rotate(diff);
        break;
      case object::t_convex :
        object::convexmesh->rotate(diff);
        break;
      case object::t_rectangle :
        object::rectanglemesh->rotate(diff);
        break;
      case object::t_sprite :
        object::sprite->rotate(diff);
        break;
      case object::t_text :
        object::textmesh->rotate(diff);
        break;
      case object::t_custom :
        object::cumesh->rotate(diff);
        break;
    }
  }

  sf::Vector2f origin = object::getPosition();
  sf::Vector2f translated;
  sf::Vector2f new_rotation;

  float rad_angle = degToRad(diff);

  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    if((*it)->getName().find("_debug_") != std::string::npos) continue;
    translated = (*it)->getPosition() - origin;
    new_rotation.x = translated.x * std::cos(rad_angle) - translated.y * std::sin(rad_angle);
    new_rotation.y = translated.x * std::sin(rad_angle) + translated.y * std::cos(rad_angle);

    new_rotation += origin;

    (*it)->setPosition(new_rotation);
    (*it)->rotate(diff);
  }

  return true;
}

bool object::rotate(float angle){
  if(angle > 360.f || angle < -360.f) return false;
  angle += object::rotation;

  if(angle > 360.f) angle -= 360.f;
  if(angle < -360.f) angle += 360.f;

  return object::setRotation(angle);
}

void object::setSize(float size){
  sf::Vector2f old_pos;
  sf::Vector2f pos;
  if(object::hasCollider()) object::collider->setSize(size);
  if(object::hasMesh()){
    switch(object::meshtype){
      case t_none : break;
      case object::t_circle:
        object::circlemesh->setScale(size, size);
        break;
      case object::t_convex:
        object::convexmesh->setScale(size, size);
        break;
      case object::t_rectangle:
        object::rectanglemesh->setScale(size, size);
        break;
      case object::t_sprite:
        object::sprite->setScale(size, size);
        break;
      case object::t_text:
        object::textmesh->setScale(size, size);
        break;
      case object::t_custom:
        object::cumesh->setScale(size, size);
        break;
    }
  }
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    if((*it)->getName().find("_debug_") != std::string::npos) continue;
    pos = (*it)->getPosition() - object::getPosition();
    old_pos = pos;
    if(object::size != 1.f){
      pos.x /= object::size;
      pos.y /= object::size;
    }
    pos.x *= size;
    pos.y *= size;

    (*it)->move(pos - old_pos);
    (*it)->setSize(size);
  }
  object::size = size;
}

void object::setScale(float size){
  object::setSize(size);
}

void object::scale(float size){
  object::setSize(object::size + size);
}

float object::getSize(){
  return object::size;
}

float object::getScale(){
  return object::size;
}

// Enabled
void object::setEnabled(bool value){
  object::m_enabled = value;
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    (*it)->setEnabled(value);
  }
}

bool object::enabled(){
  return object::m_enabled;
}

// Parent and child methods
void object::addChild(object* ob){
  object::childs.push_back(ob);
  if(!ob->hasParent(this))
    ob->addParent(this);
}

void object::addChild(int id){
  object* ob = engine->objmgr->getObjectById(id);
  if(ob) object::childs.push_back(ob);
}

void object::addChild(std::string name){
  object* ob = engine->objmgr->getObjectByName(name);
  if(ob) object::childs.push_back(ob);
}

std::vector<object*>& object::getChilds(){
  return object::childs;
}

object* object::getChild(std::string name){
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    if((*it)->getName() == name)
      return (*it);
  }

  return nullptr;
}

void object::addParent(object* ob){
  object::parent = ob;
  object::setEnabled(ob->enabled());
  if(!ob->hasChild(this)) ob->addChild(this);
}

void object::addParent(int id){
  object* ob = engine->objmgr->getObjectById(id);
  if(ob) object::addParent(ob);
}

void object::addParent(std::string name){
  object* ob = engine->objmgr->getObjectByName(name);
  if(ob) object::addParent(ob);
}

object* object::getParent(){
  return object::parent;
}

bool object::hasChild(object* ob){
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    if(ob == (*it)) return true;
  }
  return false;
}

bool object::hasChild(int id){
  return object::hasChild(engine->objmgr->getObjectById(id));
}

bool object::hasChild(std::string name){
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    if((*it)->getName() == name)
      return true;
  }
  return false;
}

bool object::hasChild(){
  return object::childs.size() > 0;
}

bool object::hasParent(object* ob){
  return (object::parent == ob);
}

bool object::hasParent(int id){
  return object::hasParent(engine->objmgr->getObjectById(id));
}

bool object::hasParent(std::string name){
  return object::parent->getName() == name;
}

bool object::hasParent(){
  if(object::parent) return true;
  return false;
}

void object::removeChilds(){
  object* ob;
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end();){
    ob = (*it);
    it = object::childs.erase(it);
    if(ob->hasParent(this)){
      ob->removeParent();
    }
  }
}

void object::removeChild(object* ob){
  for(std::vector<object*>::iterator it = object::childs.begin(); it != object::childs.end(); ++it){
    if((*it) == ob){
      object::childs.erase(it);
      if(ob->hasParent(this)){
        ob->removeParent();
      }
      break;
    }
  }
}

void object::removeChild(int id){
  object* ob = engine->objmgr->getObjectById(id);
  if(ob) object::removeChild(ob);
}

void object::removeChild(std::string name){
  object*  ob = engine->objmgr->getObjectByName(name);
  if(ob) object::removeChild(ob);
}

void object::removeParent(){
  object* parent = object::parent;
  object::parent = nullptr;
  if(parent->hasChild(this)){
    parent->removeChild(this);
  }
}

// Physics
void object::createCollider(){
  if(!object::hasCollider()){
    physicsbody* collider = static_cast<physicsbody*>(object::addComponent(new physicsbody("OtCollider")));
    object::collider = collider;
  }
}

void object::destroyCollider(){
  object::removeComponent(object::collider);
  object::collider = nullptr;
}

bool object::hasCollider(){
  return object::collider;
}

// Component methods //

// addComponent //

void object::removeComponent(std::string name){
  for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end();){
    if(name == (*it)->getName()){
      delete (*it);
      object::components.erase(it);
    } else {
      ++it;
    }
  }
}

void object::removeComponent(const char* name){
  object::removeComponent(name);
}

void object::clearComponents(){
  for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end(); ++it){
    delete (*it);
  }
  object::components.clear();
}

bool object::hasComponent(){
  return object::components.size() > 0;
}

bool object::hasComponent(component* com){
  for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end(); ++it){
    if(com == (*it))
      return true;
  }
  return false;
}

bool object::hasComponent(std::string name){
  for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end(); ++it){
    if(name == (*it)->getName()) return true;
  }
  return false;
}

component* object::getComponent(std::string name){
  for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end(); ++it){
    if(name == (*it)->getName()) return (*it);
  }
  return nullptr;
}

// Layers
bool object::setLayer(int i){
  if(engine->re->hasLayer(i)){
    object::layer = i;
    return true;
  }
  return false;
}
int object::getLayer(){
  return object::layer;
}

bool object::isAlwaysLast(){
  return object::alwayslast;
}

void object::setAlwaysLast(bool last){
  object::alwayslast = last;
}

// Get view
int object::getView(){
  return object::view;
}
// Set view
void object::setView(int i){
  object::view = i;
}

// Set custommesh //

// Loops
void object::setLoopId(unsigned int id){
  object::loop = id;
}
unsigned int object::getLoopId(){
  return object::loop;
}

// Tags
bool object::addTag(std::string name){
  for(std::vector<std::string>::iterator it = object::tags.begin(); it != object::tags.end(); ++it){
    if((*it) == name) return false;
  }
  object::tags.push_back(name);
  return true;
}

bool object::removeTag(std::string name){
  for(std::vector<std::string>::iterator it = object::tags.begin(); it != object::tags.end(); ++it){
    if((*it) == name){
      object::tags.erase(it);
      return true;
    }
  }
  return false;
}

bool object::hasTag(std::string name){
  for(std::vector<std::string>::iterator it = object::tags.begin(); it != object::tags.end(); ++it){
    if((*it) == name) return true;
  }
  return false;
}

std::vector<std::string> object::getTags(){
  return object::tags;
}

sf::Shader* object::addShader(sf::Shader* shader){
  object::shader = shader;
  return object::shader;
}

bool object::hasShader(){
  return object::shader;
}

sf::Shader* object::getShader(){
  return object::shader;
}

bool object::removeShader(){
  object::shader = nullptr;
  return true;
}

bool object::deleteShader(){
  if(object::shader){
    delete object::shader;
    return true;
  }
  return false;
}

void object::setBlendModeEnabled(bool value){
  object::blendmode_enabled = value;
}

bool object::getBlendModeEnabled(){
  return object::blendmode_enabled;
}

void object::setBlendMode(sf::BlendMode mode){
  object::blendmode = mode;
}

void object::removeBlend(){
  object::blendmode = sf::BlendNone;
}

sf::BlendMode &object::getBlendMode(){
  return object::blendmode;
}
