#include <string>
#include <vector>

#include "engine/globals.hpp"
#include "engine/objectmanager.hpp"
#include "engine/object.hpp"

int objectmanager::registerObject(object* ob){
  objectmanager::addobjects.push_back(ob);
  return 0;
}
object* objectmanager::createObject(std::string name, int loop){
  object* newob = new object(name, loop);
  if(objectmanager::registerObject(newob)){
    delete newob;
    return nullptr;
  }
  newob->setId_(objectmanager::generateId());
  objectmanager::object_count++;
  return newob;
}
// Get object using id
object* objectmanager::getObjectById(int id){
  for(std::vector<object*>::iterator it = objectmanager::objects.begin(); it != objectmanager::objects.end(); ++it){
    if((*it)->getId() == id && !(*it)->destroyThis())
      return (*it);
    }
  for(std::vector<object*>::iterator it = objectmanager::addobjects.begin(); it != objectmanager::addobjects.end(); ++it){
    if((*it)->getId() == id && !(*it)->destroyThis())
      return (*it);
    }
  return nullptr;
}

object* objectmanager::getObjectByName(std::string name){
  for(std::vector<object*>::iterator it = objectmanager::objects.begin(); it != objectmanager::objects.end(); ++it){
    if((*it)->getName() == name && !(*it)->destroyThis())
      return (*it);
  }
  for(std::vector<object*>::iterator it = objectmanager::addobjects.begin(); it != objectmanager::addobjects.end(); ++it){
    if((*it)->getName() == name && !(*it)->destroyThis())
      return (*it);
  }

  return nullptr;
}

std::vector<object*> &objectmanager::getObjectsByName(std::string name){
  objectmanager::objectsbyname.clear();

  for(std::vector<object*>::iterator it = objectmanager::objects.begin(); it != objectmanager::objects.end(); ++it){
    if((*it)->getName() == name && !(*it)->destroyThis())
      objectmanager::objectsbyname.push_back((*it));
  }
  for(std::vector<object*>::iterator it = objectmanager::addobjects.begin(); it != objectmanager::addobjects.end(); ++it){
    if((*it)->getName() == name && !(*it)->destroyThis())
      objectmanager::objectsbyname.push_back((*it));

  }

  return objectmanager::objectsbyname;
}

void objectmanager::optimizeUnusedIds(){
  for(std::vector<int>::iterator it = objectmanager::unusedids.begin(); it != objectmanager::unusedids.end();){
    if((*it) >= objectmanager::highestid){
      it = objectmanager::unusedids.erase(it);
    }else{
      it++;
    }
  }
}

int objectmanager::getNewHighestId(){
  int highest = 0;
  for(std::vector<object*>::iterator it = objectmanager::objects.begin(); it != objectmanager::objects.end(); ++it){
    if((*it)->getId() > highest)
      highest = (*it)->getId();
  }
  return highest;
}

int objectmanager::getUnusedId(){
  int id = objectmanager::unusedids.front();
  objectmanager::unusedids.erase(objectmanager::unusedids.begin());
  return id;
}

int objectmanager::generateId(){
  if(objectmanager::unusedids.size() > 0){
    return objectmanager::getUnusedId();
  }else{
    ++objectmanager::highestid;
    return objectmanager::highestid;
  }
}

void objectmanager::update(){
  // Delete objects that are marked for deletion
  for(std::vector<object*>::iterator it = objectmanager::objects.begin(); it != objectmanager::objects.end();){
    if((*it)->destroyThis()){
      if((*it)->getId() == objectmanager::highestid){
        objectmanager::highestid = objectmanager::getNewHighestId();
        objectmanager::optimizeUnusedIds();
      }
      delete (*it);
      it = objectmanager::objects.erase(it);
      objectmanager::object_count--;
    }else{
      ++it;
    }
  }
  // Add new objects to main vector
  if(objectmanager::addobjects.size() > 0){
    objectmanager::objects.reserve(objectmanager::addobjects.size());
    objectmanager::objects.insert(objectmanager::objects.end(), objectmanager::addobjects.begin(), objectmanager::addobjects.end());
    objectmanager::addobjects.clear();
  }
}

void objectmanager::updateObjects(){
  for(std::vector<qpuloop*>::iterator it0 = engine->re->loops.begin(); it0 != engine->re->loops.end(); ++it0){
    for(std::vector<object*>::iterator it1 = objectmanager::objects.begin(); it1 != objectmanager::objects.end(); ++it1){
      if(!(*it1)->destroyed() && (*it1)->enabled() && (*it0)->id == (*it1)->getLoopId() && (*it0)->enabled) (*it1)->update();
    }
  }
}

void objectmanager::fixedUpdateObjects(){
  for(std::vector<qpuloop*>::iterator it0 = engine->re->loops.begin(); it0 != engine->re->loops.end(); ++it0){
    for(std::vector<object*>::iterator it1 = objectmanager::objects.begin(); it1 != objectmanager::objects.end(); ++it1){
      if(!(*it1)->destroyed() && (*it1)->enabled() && (*it0)->id == (*it1)->getLoopId() && (*it0)->enabled) (*it1)->fixedUpdate();
    }
  }
}

std::vector<object*> &objectmanager::getObjects(){
  objectmanager::allobjects.clear();
  objectmanager::allobjects.insert(objectmanager::allobjects.end(), objectmanager::objects.begin(), objectmanager::objects.end());
  objectmanager::allobjects.insert(objectmanager::allobjects.end(), objectmanager::addobjects.begin(), objectmanager::addobjects.end());
  return objectmanager::allobjects;

}

void objectmanager::destroyAllByName(std::string name){
  std::vector<object*> objects = objectmanager::getObjectsByName(name);
  for(std::vector<object*>::iterator it = objects.begin(); it != objects.end(); ++it){
    (*it)->destroy();
  }
}

int objectmanager::getObjectCount(){
  return objectmanager::object_count;
}
