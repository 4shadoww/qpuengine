#include <vector>
#include <cmath>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Color.hpp>

#include "engine/physics.hpp"
#include "engine/globals.hpp"
#include "engine/component/physicsbody.hpp"
#include "engine/object.hpp"
#include "engine/component/raycast.hpp"
#include "engine/math.hpp"

otphysics::otphysics(){
  debugdraw = false;
}

void otphysics::update(){
  std::vector<object*> objects = engine->objmgr->getObjects();

  // Update debugdraw
  if(otphysics::debugdraw){
    for(std::vector<object*>::iterator it = objects.begin(); it != objects.end(); ++it){
      if(!(*it)->hasCollider() || (*it)->getName() == "_debug_colliderDebugMesh" || (*it)->destroyed() || !(*it)->enabled()){
        continue;
      }else if(!(*it)->hasChild("_debug_colliderDebugMesh")){
        otphysics::createDebugMesh((*it));
      }else {
        otphysics::updateDebugMesh((*it), (*it)->getChild("_debug_colliderDebugMesh"));
      }
    }
  }

  // Remove collisions
  for(std::vector<object*>::iterator it = objects.begin(); it != objects.end(); ++it){
    if((*it)->hasCollider() && !(*it)->destroyed() && (*it)->enabled()){
      (*it)->collider->clearColliding_();
      (*it)->collider->setVelocityInvalid_();
      (*it)->collider->useLastPosition_(true);
    }
  }

  // Check collision
  for(std::vector<object*>::iterator it0 = objects.begin(); it0 != objects.end(); ++it0){
    if((*it0)->hasCollider() && !(*it0)->destroyed() && (*it0)->enabled()){
      for(std::vector<object*>::iterator it1 = objects.begin(); it1 != objects.end(); ++it1){
        if(it0 == it1 || !(*it1)->hasCollider()) continue;
        if((*it0)->collider->isStatic() && (*it1)->collider->isStatic()) continue;
        if(!(*it1)->collider->isTrigger() && !(*it1)->destroyed() && (*it1)->enabled() && otphysics::shouldCheck((*it0), (*it1))){
          otphysics::checkCollision((*it0), (*it1));
        }
      }
    }
  }
}

// Check are objects possibly colliding
bool otphysics::shouldCheck(object* ob0, object* ob1){
  sf::Vector2f min0 = ob0->getPosition() + ob0->collider->getMin();
  sf::Vector2f max0 = ob0->getPosition() + ob0->collider->getMax();
  sf::Vector2f min1 = ob1->getPosition() + ob1->collider->getMin();
  sf::Vector2f max1 = ob1->getPosition() + ob1->collider->getMax();
  return (min0.x <= max1.x && max0.x >= min1.x) &&
  (min0.y <= max1.y && max0.y >= min1.y);
}

float otphysics::getGap(object* ob0, bool dynamic){
  float min = 0.f;
  float value;
  bool first = true;
  std::vector<object*> objects = engine->objmgr->getObjects();
  for(std::vector<object*>::iterator it = objects.begin(); it != objects.end(); ++it){
    if((*it)->hasCollider() && !(*it)->collider->isTrigger() && !(*it)->destroyed() && (*it)->enabled()){
      if((!dynamic && (*it)->collider->isStatic()) || (dynamic)){
        value = otphysics::checkCollision(ob0, (*it));
        if(first || value < min){
          min = value;
          first = false;
        }
      }
    }
  }
  return min;
}

// Apply MTV
// TODO
// Do this correctly
// Current way to do this isn't correct
void otphysics::applyMTV(physicsbody* sq0, physicsbody* sq1, gapaxis &min){
  if(!sq0->isTrigger() && !sq1->isTrigger()){
    if(sq0->isStatic() && sq1->isStatic()) return;

    sf::Vector2f mtv = min.axis * min.gap;
    if(qpumath::dotproduct(sq0->getCenter() - sq1->getCenter(), mtv) < 0) mtv = -mtv;

    if(sq0->isStatic()){
      sq1->updatePhyenPosition_();
      sq1->move(-mtv.x, -mtv.y);
      return;
    }
    if(sq1->isStatic()){
      sq0->updatePhyenPosition_();
      sq0->move(mtv.x, mtv.y);
      return;
    }

    sq0->updatePhyenPosition_();
    sq1->updatePhyenPosition_();

    float velocity0 = sq0->getPhyenVelocity_();
    float velocity1 = sq1->getPhyenVelocity_();
    float force0 = velocity0 * sq0->getMass() + sq0->getMass();
    float force1 = velocity1 * sq1->getMass() + sq1->getMass();
    float force_max = force0 + force1;
    float force0_factor = force0 / force_max;
    float force1_factor = force1 / force_max;

    if(std::isnan(force0_factor)) force0_factor = 0.f;
    if(std::isnan(force1_factor)) force1_factor = 0.f;

    if(force0_factor == 0 && force1_factor == 0){
      force0_factor = 0.5f;
      force1_factor = 0.5f;
    }

    sq0->move(mtv.x * force1_factor, mtv.y * force1_factor);
    sq1->move(-mtv.x * force0_factor, -mtv.y * force0_factor);
  }
}

// Collision check
float otphysics::checkCollision(object* ob0, object* ob1){
  if(ob0->collider->getType() == physicsbody::t_convex && ob1->collider->getType() == physicsbody::t_convex){
    return otphysics::convexCollision(ob0, ob1);
  } else if((ob0->collider->getType() == physicsbody::t_circle && ob1->collider->getType() == physicsbody::t_convex) || (ob0->collider->getType() == physicsbody::t_convex && ob1->collider->getType() == physicsbody::t_circle)){
    return otphysics::circleConvexCollision(ob0, ob1);
  } else if(ob0->collider->getType() == physicsbody::t_circle && ob1->collider->getType() == physicsbody::t_circle){
    return otphysics::circleCollision(ob0, ob1);
  }
  return 0;
}

// Collision check for convex shapes
float otphysics::convexCollision(object* ob0, object* ob1){
  physicsbody* sq0 = ob0->collider;
  physicsbody* sq1 = ob1->collider;

  sf::Vector2f mtv;

  gapaxis min = otphysics::projectOnAxes(sq0, sq1);
  if(min.gap > 0){
    sq0->setColliding_(false);
    return min.gap;
  }
  gapaxis min0 = otphysics::projectOnAxes(sq1, sq0);
  if(min0.gap > 0){
    sq0->setColliding_(false);
    return min0.gap;
  }

  if(min.gap < min0.gap) min = min0;

  otphysics::applyMTV(sq0, sq1, min);

  sq0->setColliding_(true);
  return min.gap;
}

// Collision check for circle and convex shape
float otphysics::circleConvexCollision(object* ob0, object* ob1){
  physicsbody* sq0;
  physicsbody* sq1;
  physicsbody* sq = ob0->collider;

  sf::Vector2f mtv;

  if(ob0->collider->getType() == physicsbody::t_circle){
    sq0 = ob0->collider;
    sq1 = ob1->collider;
  }else{
    sq0 = ob1->collider;
    sq1 = ob0->collider;
  }

  gapaxis min = otphysics::projectToCircle(sq0, sq1);
  if(min.gap > 0.f){
    sq->setColliding_(false);
    return min.gap;
  }
  gapaxis min0 = otphysics::projectOnCircle(sq0, sq1);

  if(min0.gap > 0.f){
    sq->setColliding_(false);
    return min0.gap;
  }

  if(min.gap < min0.gap) min = min0;

  otphysics::applyMTV(sq0, sq1, min);

  sq->setColliding_(true);
  return min.gap;
}

gapaxis otphysics::projectOnCircle(physicsbody* sq0, physicsbody* sq1){
  float gap;
  sf::Vector2f axis;
  projection proj0;
  projection proj1;
  gapaxis min;

  // Edge calculations
  float angle;
  sf::Vector2f vec0;
  sf::Vector2f vec1;

  for(unsigned int i = 0; i < sq1->getVerticleCount(); i++){
    angle = qpumath::getAngle(sq1->getVerticlePosition(i) - sq0->getPosition());

    angle = qpumath::fixedAngle(angle + 90.f);

    vec0 = qpumath::getEndPoint(sq0->getPosition(), angle, sq0->getRadius());
    vec1 = qpumath::getEndPoint(sq0->getPosition(), angle, -sq0->getRadius());

    axis = qpumath::getAxis(vec0, vec1);

    proj0 = otphysics::projectCircle(axis, vec0, vec1);
    proj1 = otphysics::project(axis, sq1);
    gap = otphysics::getGap(proj0, proj1) - sq0->getRadius();

    if(gap > 0){
      min.gap = gap;
      return min;
    }

    if(i == 0  || gap > min.gap){
      min.gap = gap;
      min.axis = axis;
    }
  }
  return min;
}

gapaxis otphysics::projectToCircle(physicsbody* sq0, physicsbody* sq1){
  float gap;
  sf::Vector2f axis;
  projection proj0;
  projection proj1;
  gapaxis min;

  // Edge calculations
  float angle;
  sf::Vector2f vec0;
  sf::Vector2f vec1;

  for(unsigned int i = 0; i < sq1->getVerticleCount(); i++){
    angle = qpumath::getAngle(sq1->getVerticlePosition(i) - sq1->getVerticlePosition((i+1) % sq1->getVerticleCount()));
    angle = qpumath::fixedAngle(angle + 90.f);
    vec0 = qpumath::getEndPoint(sq0->getPosition(), angle, sq0->getRadius());
    vec1 = qpumath::getEndPoint(sq0->getPosition(), angle, -sq0->getRadius());

    axis = qpumath::getAxis(sq1->getVerticlePosition(i), sq1->getVerticlePosition((i+1) % sq1->getVerticleCount()));

    proj0 = otphysics::projectCircle(axis, vec0, vec1);
    proj1 = otphysics::project(axis, sq1);
    gap = otphysics::getGap(proj0, proj1);

    if(gap > 0){
      min.gap = gap;
      return min;
    }

    if(i == 0  || gap > min.gap){
      min.gap = gap;
      min.axis = axis;
    }
  }
  return min;
}

// Collision check for circles
float otphysics::circleCollision(object* ob0, object* ob1){
  physicsbody* sq0 = ob0->collider;
  physicsbody* sq1 = ob1->collider;

  sf::Vector2f mtv;

  gapaxis min = otphysics::circleProjection(sq0, sq1);
  if(min.gap > 0.f){
    sq0->setColliding_(false);
    return min.gap;
  }
  gapaxis min0 = otphysics::circleProjection(sq1, sq0);

  if(min0.gap > 0.f){
    sq0->setColliding_(false);
    return min0.gap;
  }

  if(min.gap < min0.gap) min = min0;

  otphysics::applyMTV(sq0, sq1, min);

  sq0->setColliding_(true);
  return min.gap;
}

gapaxis otphysics::circleProjection(physicsbody* sq0, physicsbody* sq1){
  float gap;
  sf::Vector2f axis;
  projection proj0;
  projection proj1;
  gapaxis min;

  // Edge calculations
  float angle;
  sf::Vector2f axis0;
  sf::Vector2f axis1;
  sf::Vector2f vec0;
  sf::Vector2f vec1;
  sf::Vector2f vec2;
  sf::Vector2f vec3;

  angle = qpumath::getAngle(sq1->getPosition() - sq0->getPosition());

  vec0 = qpumath::getEndPoint(sq0->getPosition(), angle, sq0->getRadius());
  vec1 = qpumath::getEndPoint(sq0->getPosition(), angle, -sq0->getRadius());
  vec2 = qpumath::getEndPoint(sq1->getPosition(), angle, sq1->getRadius());
  vec3 = qpumath::getEndPoint(sq1->getPosition(), angle, -sq1->getRadius());

  angle = qpumath::fixedAngle(angle + 90.f);
  axis0 = qpumath::getEndPoint(sq0->getPosition(), angle, sq0->getRadius());
  axis1 = qpumath::getEndPoint(sq0->getPosition(), angle, -sq0->getRadius());

  axis = qpumath::getAxis(axis0, axis1);

  proj0 = otphysics::projectCircle(axis, vec0, vec1);
  proj1 = otphysics::projectCircle(axis, vec2, vec3);

  gap = otphysics::getGap(proj0, proj1);

  min.gap = gap;
  min.axis = axis;

  return min;
}

gapaxis otphysics::projectOnAxes(physicsbody* sq0, physicsbody* sq1){
  float gap;
  sf::Vector2f axis;
  projection proj0;
  projection proj1;
  gapaxis min;

  for(unsigned int i = 0; i < sq0->getVerticleCount(); i++){
    // Calculate axis
    axis = qpumath::getAxis(sq0->getVerticlePosition(i), sq0->getVerticlePosition((i+1) % sq0->getVerticleCount()));
    // Project
    proj0 = otphysics::project(axis, sq0);
    proj1 = otphysics::project(axis, sq1);
    gap = otphysics::getGap(proj0, proj1);

    if(gap > 0){
      min.gap = gap;
      return min;
    }

    if(i == 0  || gap > min.gap){
      min.gap = gap;
      min.axis = axis;
    }
  }
  return min;
}

projection otphysics::project(sf::Vector2f axis, physicsbody* sq){
  projection values;
  float dp;

  for(unsigned int i = 0; i < sq->getVerticleCount(); i++){
    dp = qpumath::dotproduct(axis, sq->getVerticlePosition(i));
    if(i == 0){
      values.min = dp;
      values.max = dp;
    }else if(dp < values.min){
      values.min = dp;
    }else if(dp > values.max){
      values.max = dp;
    }
  }
  return values;
}

projection otphysics::projectCircle(sf::Vector2f axis, sf::Vector2f vec0, sf::Vector2f vec1){
  projection values;
  float dp;

  dp = qpumath::dotproduct(axis, vec0);

  values.min = dp;
  values.max = dp;

  dp = qpumath::dotproduct(axis, vec1);

  if(dp < values.min){
    values.min = dp;
  }else if(dp > values.max){
    values.max = dp;
  }
  return values;
}

float otphysics::getGap(projection gap0, projection gap1){
  if(gap0.min < gap1.min) return gap1.min - gap0.max;
  return gap0.min - gap1.max;
}

// Raycast methods
bool otphysics::inRange(object* ob0, object* ob1, float magnitude){
  sf::Vector2f min = ob1->collider->getMin();
  sf::Vector2f max = ob1->collider->getMax();
  min.x = std::fabs(min.x);
  min.y = std::fabs(min.y);
  max.x = std::fabs(max.x);
  max.y = std::fabs(max.y);
  if(min.x > max.x) max.x = min.x;
  if(min.y > max.y) max.y = min.y;
  if(max.x < max.y) max.x = max.y;
  return (qpumath::getDistance(ob0->getPosition(), ob1->getPosition()) <= magnitude + max.x);
}

rayin otphysics::convexRayIntersection(sf::Vector2f &origin, sf::Vector2f &end, physicsbody* col){
  rayin min;
  rayin current;
  for(unsigned int i = 0; i < col->getVerticleCount(); i++){
    current.position = qpumath::getLinesIntersection(origin, end, col->getVerticlePosition(i), col->getVerticlePosition((i+1) % col->getVerticleCount()));
    if(current.position.x == FLT_MAX && current.position.y == FLT_MAX) current.distance = FLT_MAX;
    else current.distance = qpumath::getDistance(origin, current.position);
    if(min.distance > current.distance) min = current;
  }
  if(min.distance != FLT_MAX) min.collider = col;
  return min;
}

rayin otphysics::circleRayIntersection(sf::Vector2f &origin, sf::Vector2f &end, physicsbody* col, raycast* ray){
  rayin min;
  min.position = qpumath::getCircleAndLineIntersection(origin, end, ray->getAngle(), ray->getMagnitude(), col->getPosition(), col->getRadius());
  min.distance = qpumath::getDistance(origin, min.position);
  if(min.distance != FLT_MAX) min.collider = col;
  return min;
}

rayin otphysics::rayIntersection(sf::Vector2f &origin, sf::Vector2f &end, object* ob, raycast* ray){
  physicsbody* col = ob->collider;
  rayin min;

  switch(col->getType()){
    case physicsbody::t_convex:
      min = otphysics::convexRayIntersection(origin, end, col);
      break;
    case physicsbody::t_circle:
      min = otphysics::circleRayIntersection(origin, end, col, ray);
      break;
  }
  return min;
}

void otphysics::updateRaycast(raycast* ray){
  std::vector<object*> objects = engine->objmgr->getObjects();
  object* ob = ray->getHostObject();

  sf::Vector2f ray_origin = ob->getPosition();
  sf::Vector2f ray_end = qpumath::getEndPoint(ray_origin, ray->getAngle(), ray->getMagnitude());
  rayin min;
  rayin current;

  for(std::vector<object*>::iterator it = objects.begin(); it != objects.end(); ++it){
    if(!(*it)->destroyed() && (*it)->enabled() && ob != (*it) && (*it)->hasCollider() && otphysics::inRange(ob, (*it), ray->getMagnitude())){
      current = otphysics::rayIntersection(ray_origin, ray_end, (*it), ray);
      if(min.distance > current.distance) min = current;
    }
  }
  ray->updateRay_(min.distance, min.position, min.collider);
}

// Debug draw
bool otphysics::shouldUpdate(object* ob0, object* ob1){
  if(ob0->collider->getVerticleCount() != ob1->convexmesh->getPointCount()){
    return true;
  }
  for(unsigned int i = 0; i < ob0->collider->getVerticleCount(); i++){
    if(ob0->collider->getVerticleLocalPosition(i) != ob1->convexmesh->getPoint(i)) return true;
  }

  return false;
}

void otphysics::updateDebugMesh(object* ob0, object* ob1){
  if(ob0->collider->getType() == physicsbody::t_convex && otphysics::shouldUpdate(ob0, ob1)){
    int i = 0;
    std::vector<sf::Vector2f> &verticles = ob0->collider->getVerticles();
    if(verticles.size() != ob1->convexmesh->getPointCount())
      ob1->convexmesh->setPointCount(verticles.size());

    for(std::vector<sf::Vector2f>::iterator it = verticles.begin(); it != verticles.end(); ++it){
      ob1->convexmesh->setPoint(i, (*it));
      ++i;
    }
  }else if(ob0->collider->getType() == physicsbody::t_circle && ob0->collider->getRadius() != ob1->circlemesh->getRadius()){
      ob1->circlemesh->setRadius(ob0->collider->getRadius());
      ob1->circlemesh->setOrigin(ob0->collider->getRadius(), ob0->collider->getRadius());
  }
}

void otphysics::createDebugMesh(object* ob){
  object* dob = engine->objmgr->createObject("_debug_colliderDebugMesh");
  dob->setPosition(ob->getPosition());
  dob->addParent(ob);
  dob->setAlwaysLast(true);
  if(ob->collider->getType() == physicsbody::t_convex){
    dob->createConvexMesh();
    dob->convexmesh->setFillColor(sf::Color::Transparent);
    dob->convexmesh->setOutlineColor(sf::Color::Green);
    dob->convexmesh->setOutlineThickness(-1.f);
  } else if(ob->collider->getType() == physicsbody::t_circle){
    dob->createCircleMesh();
    dob->circlemesh->setFillColor(sf::Color::Transparent);
    dob->circlemesh->setOutlineColor(sf::Color::Green);
    dob->circlemesh->setOutlineThickness(-1.f);
  }
  otphysics::updateDebugMesh(ob, dob);
}

void otphysics::createDebugMeshes(){
  std::vector<object*> objects = engine->objmgr->getObjects();
  for(std::vector<object*>::iterator it = objects.begin(); it != objects.end(); ++it){
    if((*it)->hasCollider() && !(*it)->destroyThis() && !(*it)->hasChild("_debug_colliderDebugMesh")){
      otphysics::createDebugMesh((*it));
      }
  }
}

void otphysics::destroyDebugMeshes(){
  engine->objmgr->destroyAllByName("_debug_colliderDebugMesh");
}

void otphysics::enableDebugDraw(){
  otphysics::createDebugMeshes();
  otphysics::debugdraw = true;
}

void otphysics::disableDebugDraw(){
  otphysics::destroyDebugMeshes();
  otphysics::debugdraw = false;
}

bool otphysics::debugDrawEnabled(){
  return otphysics::debugdraw;
}
