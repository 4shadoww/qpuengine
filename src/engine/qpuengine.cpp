#include <limits.h>
#include <unistd.h>
#include <libgen.h>

#include "engine/qpuengine.hpp"

qpuengine* qpuengine::instance = 0;

qpuengine* qpuengine::getInstance(){
  if(instance == 0){
    instance = new qpuengine();
  }
  return instance;
}
qpuengine::qpuengine(){}

void qpuengine::init(){
  qpuengine::wm = new window(800, 600, "qpuengine");
  qpuengine::wm->create();
  qpuengine::re = new renderer(qpuengine::wm->sfwindow);
  qpuengine::objmgr = new objectmanager;
  qpuengine::entitymgr = new entitymanager();
  qpuengine::phyen = new otphysics();
  qpuengine::fontmgr = new fontloader;
  qpuengine::inputmgr = new input_manager;

  // Set up logging
  qpuengine::qpulog = new logging;
  qpuengine::debug = qpuengine::qpulog->create("debug");

  qpuengine::debugger = new qput::debugger;

  // Get path of the executable
  // Works only with linux
  char result[PATH_MAX] = {};
  readlink("/proc/self/exe", result, PATH_MAX);
  qpuengine::path = std::string(dirname(result))+"/";
}

void qpuengine::startLoop(){
qpuengine::re->render();
}

float qpuengine::deltaTime(){
  return qpuengine::re->deltatime.asSeconds();
}

float qpuengine::fixedDeltaTime(){
  return qpuengine::re->fixeddeltatime.asSeconds();
}

std::string qpuengine::getPath(){
  return qpuengine::path;
}

bool qpuengine::hasFocus(){
  return wm->hasFocus();
}

bool qpuengine::addLoop(unsigned int id, bool enabled){
  return qpuengine::re->addLoop(id, enabled);
}

qpuloop* qpuengine::getLoop(unsigned int id){
  return qpuengine::re->getLoop(id);
}

bool qpuengine::loopEnabled(unsigned int id){
  return qpuengine::re->loopEnabled(id);
}

bool qpuengine::loopExists(unsigned int id){
  return qpuengine::re->loopExists(id);
}

bool qpuengine::removeLoop(unsigned int id){
  return qpuengine::re->removeLoop(id);
}

object* qpuengine::createObject(std::string name, int loop){
  return qpuengine::objmgr->createObject(name, loop);
}

// Input
bool qpuengine::keyPressed(sf::Keyboard::Key key){
  return qpuengine::inputmgr->keyPressed(key);
}

bool qpuengine::keyPressed(std::string name){
  return qpuengine::inputmgr->keyPressed(name);
}

sf::Vector2i qpuengine::getMouseRawPos(){
  return qpuengine::inputmgr->getMouseRawPos();
}

sf::Vector2i qpuengine::getMousePos(){
  return qpuengine::inputmgr->getMousePos();
}

sf::Vector2i qpuengine::getMousePos(sf::RenderWindow* window){
  return qpuengine::inputmgr->getMousePos(window);
}

sf::Vector2f qpuengine::getMouseGlobalPos(){
  return qpuengine::inputmgr->getMouseGlobalPos();
}

sf::Vector2f qpuengine::getMouseGlobalPos(sf::Vector2f relative){
  return qpuengine::inputmgr->getMouseGlobalPos(relative);
}

sf::Vector2f qpuengine::getMouseGlobalPos(sf::RenderWindow* window){
  return qpuengine::inputmgr->getMouseGlobalPos(window);
}

sf::Vector2f qpuengine::getMouseGlobalPos(sf::RenderWindow* window, sf::Vector2f relative){
  return qpuengine::inputmgr->getMouseGlobalPos(window, relative);
}
