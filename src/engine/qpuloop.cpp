#include "engine/qpuloop.hpp"

qpuloop::qpuloop(unsigned int id){
  qpuloop::id = id;
  qpuloop::enabled = true;
}

qpuloop::qpuloop(unsigned int id, bool enabled){
  qpuloop::id = id;
  qpuloop::enabled = enabled;
}
