#include <SFML/Window/Event.hpp>

#include "engine/renderer.hpp"
#include "engine/globals.hpp"
#include "engine/object.hpp"
#include "engine/custommesh.hpp"
#include "engine/qpuloop.hpp"

renderer::renderer(sf::RenderWindow* sfwindow){
  renderer::sfwindow = sfwindow;
  // Init camera
  renderer::camera = new sf::View;
  renderer::camera->reset(sf::FloatRect(0, 0, engine->wm->getWidth(), engine->wm->getHeight()));
  renderer::views.push_back(renderer::camera);
  renderer::hudcamera = new sf::View;
  renderer::hudcamera->reset(sf::FloatRect(0, 0, engine->wm->getWidth(), engine->wm->getHeight()));
  renderer::views.push_back(renderer::hudcamera);
  // Init layers
  renderer::layers.push_back(0);
  // Init object update loops
  renderer::addLoop(0, true);
}

bool renderer::render(){
  sf::Clock deltaclock;
  sf::Clock fixeddeltaclock;
  sf::Clock frameclock;
  sf::Event event;
  const sf::Time updaterate = sf::seconds(1.f / 75.f);
  sf::Time lastupdate = sf::Time::Zero;
  //renderer::sfwindow->setFramerateLimit(render::framerate);
  renderer::sfwindow->setVerticalSyncEnabled(true);
  // Init
  engine->entitymgr->initEntity();

  while(renderer::sfwindow->isOpen()){
    lastupdate += frameclock.restart();
    renderer::deltatime = deltaclock.restart();

    while(lastupdate >= updaterate){
      renderer::fixeddeltatime = lastupdate - (lastupdate - updaterate);
      lastupdate -= updaterate;
      engine->entitymgr->initEntity();
      engine->entitymgr->fixedUpdate();

      engine->objmgr->update();
      engine->objmgr->fixedUpdateObjects();

      engine->phyen->update();
    }

    while(renderer::sfwindow->pollEvent(event)){
      if(event.type == sf::Event::Closed) renderer::sfwindow->close();
      if(event.type == sf::Event::GainedFocus) engine->wm->setFocus_(true);
      else if(event.type == sf::Event::LostFocus) engine->wm->setFocus_(false);
    }
    engine->entitymgr->update();
    engine->objmgr->updateObjects();
    engine->debugger->update();
    renderer::sfwindow->clear();
    renderer::drawObjects();
    renderer::sfwindow->display();
  }

  return true;
}

void renderer::drawObject(object* ob){
  switch(ob->meshtype){
    case object::t_none : break;
    case object::t_circle:
      renderer::sfwindow->draw(*ob->circlemesh);
      break;
    case object::t_convex:
      renderer::sfwindow->draw(*ob->convexmesh);
      break;
    case object::t_rectangle:
      renderer::sfwindow->draw(*ob->rectanglemesh);
      break;
    case object::t_sprite:
      renderer::sfwindow->draw(*ob->sprite);
      break;
    case object::t_text:
      renderer::sfwindow->draw(*ob->textmesh);
      break;
    case object::t_custom:
      ob->cumesh->update();
      renderer::sfwindow->draw(*ob->cumesh);
      break;
  }
}

void renderer::drawObjectWithBlend(object* ob){
  switch(ob->meshtype){
    case object::t_none : break;
    case object::t_circle:
      renderer::sfwindow->draw(*ob->circlemesh, ob->getBlendMode());
      break;
    case object::t_convex:
      renderer::sfwindow->draw(*ob->convexmesh, ob->getBlendMode());
      break;
    case object::t_rectangle:
      renderer::sfwindow->draw(*ob->rectanglemesh, ob->getBlendMode());
      break;
    case object::t_sprite:
      renderer::sfwindow->draw(*ob->sprite, ob->getBlendMode());
      break;
    case object::t_text:
      renderer::sfwindow->draw(*ob->textmesh, ob->getBlendMode());
      break;
    case object::t_custom:
      ob->cumesh->update();
      renderer::sfwindow->draw(*ob->cumesh, ob->getBlendMode());
      break;
  }
}

void renderer::drawObjectWithShader(object* ob){
  switch(ob->meshtype){
    case object::t_none : break;
    case object::t_circle:
      renderer::sfwindow->draw(*ob->circlemesh, ob->getShader());
      break;
    case object::t_convex:
      renderer::sfwindow->draw(*ob->convexmesh, ob->getShader());
      break;
    case object::t_rectangle:
      renderer::sfwindow->draw(*ob->rectanglemesh, ob->getShader());
      break;
    case object::t_sprite:
      renderer::sfwindow->draw(*ob->sprite, ob->getShader());
      break;
    case object::t_text:
      renderer::sfwindow->draw(*ob->textmesh, ob->getShader());
      break;
    case object::t_custom:
      ob->cumesh->update();
      renderer::sfwindow->draw(*ob->cumesh, ob->getShader());
      break;
  }
}

// Draw objects
void renderer::drawObjects(){
  // Set view
  for(int i = 0; i < static_cast<int>(renderer::views.size()); i++){
    renderer::sfwindow->setView((*renderer::views.at(i)));

    // Draw object layers
    for(std::vector<int>::iterator cl = renderer::layers.begin(); cl != renderer::layers.end(); cl++){
      for(std::vector<object*>::iterator it = engine->objmgr->objects.begin(); it != engine->objmgr->objects.end(); ++it){
        if(!(*it)->destroyed() && (*it)->enabled() && (*it)->hasMesh() && i == (*it)->getView() && !(*it)->isAlwaysLast() && (*it)->getLayer() == (*cl)){
          if((*it)->hasShader()) renderer::drawObjectWithShader((*it));
          else if((*it)->getBlendModeEnabled()) renderer::drawObjectWithBlend((*it));
          else renderer::drawObject((*it));
        }
      }
    }
    // Draw always last objects
    for(std::vector<object*>::iterator it = engine->objmgr->objects.begin(); it != engine->objmgr->objects.end(); ++it){
      if(!(*it)->destroyed() && (*it)->enabled() && (*it)->hasMesh() && i == (*it)->getView() && (*it)->isAlwaysLast()){
        if((*it)->hasShader()) renderer::drawObjectWithShader((*it));
        else if((*it)->getBlendModeEnabled()) renderer::drawObjectWithBlend((*it));
        else renderer::drawObject((*it));
      }
    }
  }
}

// Camera movement
void renderer::moveCam(float x, float y){
  renderer::camera->move(x, y);
}

void renderer::moveCam(sf::Vector2f vec){
  renderer::camera->move(vec);
}

void renderer::setCamPosition(float x, float y){
  renderer::camera->setCenter(x, y);
}

void renderer::setCamPosition(sf::Vector2f vec){
  renderer::camera->setCenter(vec);
}

sf::Vector2f renderer::getCamPosition(){
  sf::Vector2f size = renderer::camera->getSize();
  size.x /= 2;
  size.y /= 2;
  return renderer::camera->getCenter() + size;
}

bool renderer::addLayer(int i){
  for(std::vector<int>::iterator it = renderer::layers.begin(); it != renderer::layers.end(); it++){
    if((*it) == i) return false;
  }
  renderer::layers.push_back(i);
  std::sort(renderer::layers.begin(), renderer::layers.end());
  return true;
}

bool renderer::removeLayer(int i){
  if(i == 0) return false;

  for(std::vector<int>::iterator it = renderer::layers.begin(); it != renderer::layers.end(); it++){
    if((*it) == i){
      renderer::layers.erase(it);
      return true;
    }
  }
  return false;
}

bool renderer::hasLayer(int i){
  for(std::vector<int>::iterator it = renderer::layers.begin(); it != renderer::layers.end(); it++){
    if((*it) == i) return true;
  }
  return false;
}

std::vector<int>& renderer::getLayers(){
  return renderer::layers;
}

sf::View* renderer::getView(int index){
  return renderer::views.at(index);
}

int renderer::addView(sf::View* view){
  int index = renderer::views.size();
  renderer::views.push_back(view);
  return index;
}

void renderer::removeView(sf::View* view){
  for(std::vector<sf::View*>::iterator it = renderer::views.begin(); it != renderer::views.end(); ++it){
    if((*it) == view){
      delete view;
      renderer::views.erase(it);
    }
  }
}

void renderer::removeView(int index){
  delete renderer::views.at(index);
  renderer::views.erase(renderer::views.begin()+index);
}

bool renderer::addLoop(unsigned int id, bool enabled){
  for(std::vector<qpuloop*>::iterator it = renderer::loops.begin(); it != renderer::loops.end(); ++it){
    if(id == (*it)->id) return false;
    if(id > (*it)->id){
      renderer::loops.insert(it-1, new qpuloop(id, enabled));
      return true;
    }
  }
  renderer::loops.push_back(new qpuloop(id, enabled));
  return true;
}

qpuloop* renderer::getLoop(unsigned int id){
  if(id < renderer::loops.size()){
    for(std::vector<qpuloop*>::iterator it = renderer::loops.begin(); it != renderer::loops.end(); ++it){
      if(id == (*it)->id) return (*it);
    }
  }
  return nullptr;
}

bool renderer::loopEnabled(unsigned int id){
  for(std::vector<qpuloop*>::iterator it = renderer::loops.begin(); it != renderer::loops.end(); ++it){
    if(id == (*it)->id) return (*it)->enabled;
  }
  return false;
}

bool renderer::loopExists(unsigned int id){
  for(std::vector<qpuloop*>::iterator it = renderer::loops.begin(); it != renderer::loops.end(); ++it){
    if(id == (*it)->id) return true;
  }
  return false;
}

bool renderer::removeLoop(unsigned int id){
  for(std::vector<qpuloop*>::iterator it = renderer::loops.begin(); it != renderer::loops.end(); ++it){
    if(id == (*it)->id){
      delete (*it);
      renderer::loops.erase(it);
      return true;
    }
  }
  return false;
}

unsigned int renderer::getFramerate(){
  return renderer::framerate;
}

void renderer::setFramerate(unsigned int fr){
  renderer::framerate = fr;
}
