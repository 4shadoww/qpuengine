#include <string>

#include <SFML/Graphics/RenderWindow.hpp>

#include "engine/window.hpp"

window::~window(){
  delete window::sfwindow;
}

window::window(int width, int height, std::string title){
  window::width = width;
  window::height = height;
  window::title = title;
}
window::window(int width, int height){
  window::width = width;
  window::height = height;
  window::title = "qpuengine";
}
window::window(std::string title){
  window::width = 1024;
  window::height = 768;
  window::title = title;
}
window::window(){
  window::width = 1024;
  window::height = 768;
  window::title = "qpuengine";
}

int window::getWidth(){
  return window::width;
}

int window::getHeight(){
  return window::height;
}

sf::Vector2i window::getSize(){
  return sf::Vector2i(window::width, window::height);
}

int window::create(){
  window::sfwindow = new sf::RenderWindow(sf::VideoMode(window::width, window::height), window::title, sf::Style::Close);
  return 0;
}

void window::setFocus_(bool value){
  window::hasfocus = value;
}

bool window::hasFocus(){
  return window::hasfocus;
}
