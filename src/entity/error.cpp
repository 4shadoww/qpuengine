#include "entity/error.hpp"
#include "engine/globals.hpp"
#include "engine/customvertex/snow_vertex.hpp"

void error_entity::init(){
  object* snow = engine->createObject("snow_vertex");
  snow->setCustomMesh(new snow_vertex);
}
