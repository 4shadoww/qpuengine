#include <SFML/Graphics.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics/BlendMode.hpp>

#include <vector>

#include "entity/testentity.hpp"
#include "engine/globals.hpp"

#include "game/maploader.hpp"
#include "game/component/player.hpp"
#include "game/pathfinder/enviroment.hpp"
#include "game/component/npc.hpp"
#include "game/pathfinder/agent.hpp"
#include "game/component/simple_enemy.hpp"

#include "engine/customvertex/snow_vertex.hpp"
#include "game/magic/particles/fire_particle.hpp"

#include "engine/math.hpp"
#include "engine/component/raycast.hpp"
#include "engine/customvertex/line.hpp"

void testentity::init(){
  engine->re->addLayer(-1);
  maploader ml;
  ml.loadMap("maps/test.json", 0, 0);

  engine->fontmgr->loadFont("fonts/DejaVuSans.ttf");

  ob0 = engine->objmgr->createObject("player");
  ob0->addComponent(new player("player"));
  ob0->setPosition(300, 300);
  ob0->collider->setMass(15.f);

  object* ob2 = engine->createObject();
  ob2->setPosition(300, 340);
  ob2->setCustomMesh(new fire_particle)->create();
  ob2->addParent(ob0);

  // ob1 = engine->createObject();
  // //ob1->addComponent(new simple_enemy);
  // ob1->createCollider();
  // ob1->setPosition(300, 330);
  // ob1->collider->setMass(15.f);

  object* snow = engine->createObject("snow");
  snow->setCustomMesh(new snow_vertex)->create();

}

void testentity::fixedUpdate(){
  //engine->debug->info("velocity:", ob0->collider->getVelocity(), "acceleration:", ob0->collider->getAcceleration());
}
