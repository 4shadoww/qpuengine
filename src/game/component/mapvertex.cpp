#include "game/component/mapvertex.hpp"

int mapvertex::numberOfTiles(Json::Value &data){
  int i = 0;
  for(Json::Value::const_iterator it = data.begin(); it != data.end(); ++it){
    if((*it).asInt() > 0) i++;
  }
  return i;
}

bool mapvertex::load(sf::Texture* texture, int tilewidth, Json::Value &data, unsigned int width, unsigned int height, sf::Vector2f offset){
  unsigned int xPos = 0;
  unsigned int yPos = 0;

    m_texture = (*texture);

    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(width * height * 4);

  for(Json::Value::const_iterator it = data.begin(); it != data.end(); ++it){
    if(xPos == width){
      xPos = 0;
      yPos++;
    }

        int tileNumber = (*it).asInt();
    if(tileNumber > 0){
          int tu = (tileNumber - 1) % (m_texture.getSize().x / tilewidth);
          int tv = (tileNumber - 1) / (m_texture.getSize().x / tilewidth);

          sf::Vertex* quad = &m_vertices[(yPos + xPos * width) * 4];

          quad[0].position = sf::Vector2f(xPos * tilewidth, yPos * tilewidth);
          quad[1].position = sf::Vector2f((xPos + 1) * tilewidth, yPos * tilewidth);
          quad[2].position = sf::Vector2f((xPos + 1) * tilewidth, (yPos + 1) * tilewidth);
          quad[3].position = sf::Vector2f(xPos * tilewidth, (yPos + 1) * tilewidth);

          quad[0].texCoords = sf::Vector2f(tu * tilewidth, tv * tilewidth);
          quad[1].texCoords = sf::Vector2f((tu + 1) * tilewidth, tv * tilewidth);
          quad[2].texCoords = sf::Vector2f((tu + 1) * tilewidth, (tv + 1) * tilewidth);
          quad[3].texCoords = sf::Vector2f(tu * tilewidth, (tv + 1) * tilewidth);
    }
    xPos++;
  }
    return true;
}
