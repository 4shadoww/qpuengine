#include "game/component/npc.hpp"

#include <cmath>

#include "engine/globals.hpp"
#include "engine/math.hpp"

nonplayer::nonplayer(std::string name) : component(name){}
nonplayer::nonplayer() : component("npc"){}

void nonplayer::fixedUpdate(){
  if(nonplayer::path_found){
    if(nonplayer::agent.path.size() != 0 && nonplayer::current_node != nonplayer::agent.path.size()-1 &&
    fabsf(host->getPosition().x - nonplayer::agent.getNodePosition(nonplayer::current_node).x) <= nonplayer::velocity &&
    fabsf(host->getPosition().y - nonplayer::agent.getNodePosition(nonplayer::current_node).y) <= nonplayer::velocity){
      nonplayer::current_node++;
    }
    if(nonplayer::current_node == nonplayer::agent.path.size()){
      nonplayer::path_found = false;
      nonplayer::current_node = 0;
      return;
    }
    sf::Vector2f node_position(nonplayer::agent.getNodePosition(nonplayer::current_node));
    float angle = qpumath::getAngle(node_position - host->getPosition());
    sf::Vector2f position = qpumath::getEndPoint(host->getPosition(), angle, nonplayer::velocity * 0.1);
    host->setPosition(position);
  }
}

bool nonplayer::move(float x, float y){
  sf::Vector2i node = nonplayer::agent.getClosestNode(x, y);
  return move2Node(node.x, node.y);
}

bool nonplayer::move(sf::Vector2f position){
  sf::Vector2i node = nonplayer::agent.getClosestNode(position.x, position.y);
  return move2Node(node.x, node.y);
}

bool nonplayer::move2Node(int x, int y){
  nonplayer::current_node = 0;
  nonplayer::path_found = nonplayer::agent.calculatePath(nonplayer::agent.getClosestNode(host->getPosition()), sf::Vector2i(x, y));
  return nonplayer::path_found;
}

void nonplayer::setEnviroment(pathfinden* env){
  nonplayer::agent.setEnviroment(env);
}

void nonplayer::setCollider(object* ob){
  nonplayer::agent.setCollider(ob);
}

void nonplayer::enableCollision(bool value){
  nonplayer::agent.enableCollision(value);
}

void nonplayer::debug(){
  nonplayer::agent.debug();
}

sf::Vector2i nonplayer::getClosestNode(sf::Vector2f position){
  return nonplayer::agent.getClosestNode(position);
}

sf::Vector2i nonplayer::getClosestNode(int x, int y){
  return nonplayer::agent.getClosestNode(x, y);
}
