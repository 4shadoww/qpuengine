#include <string>

#include <SFML/Window/Keyboard.hpp>

#include "game/component/player.hpp"
#include "engine/globals.hpp"
#include "engine/object.hpp"
#include "engine/component/physicsbody.hpp"
#include "game/magic/spell_caster.hpp"
#include "game/magic/test/fire_spell.hpp"

#include "engine/math.hpp"

fire_spell* spell = new fire_spell;

player::player(std::string name) : component(name){}

void player::init(){
  if(!host->hasCollider()){
    host->createCollider();
    host->collider->setType(physicsbody::t_circle);
    host->collider->setRadius(16);
  }
  host->createConvexMesh();
  player::caster = player::host->addComponent(new spell_caster);
  player::caster->setDistance(16.f);
  host->addTag("visible");
}

void player::fixedUpdate(){
  engine->re->setCamPosition(player::host->collider->getPosition());
  if(engine->hasFocus() && engine->keyPressed("mov_down")){
    player::host->move(0, 8 * engine->fixedDeltaTime() * 10);
  }
  if(engine->hasFocus() && engine->keyPressed("mov_right")){
    player::host->move(8 * engine->fixedDeltaTime() * 10, 0);
  }
  if(engine->hasFocus() && engine->keyPressed("mov_up")){
    player::host->move(0, -8 * engine->fixedDeltaTime() * 10);
  }
  if(engine->hasFocus() && engine->keyPressed("mov_left")){
    player::host->move(-8 * engine->fixedDeltaTime() * 10, 0);
  }
  if(engine->hasFocus() && engine->keyPressed(sf::Keyboard::Num1)){
    caster->castSpell(spell);
  }
  if(engine->hasFocus() && engine->keyPressed(sf::Keyboard::Num2)){
    host->destroy();
  }
  if(engine->hasFocus() && engine->keyPressed(sf::Keyboard::Num3)){
    host->scale(0.01);
  }
  if(engine->hasFocus() && engine->keyPressed(sf::Keyboard::Num4)){
    host->scale(-0.01);
  }

  // if(engine->hasFocus()){
  //   float angle = qpumath::getAngle(host->getPosition() - engine->getMouseGlobalPos());
  //   sf::Vector2f vec = qpumath::angleToVector(angle);
  //   engine->debug->info(angle);
  //   engine->debug->info(vec.x, vec.y);
  //   engine->debugger->pingPosition(engine->getMouseGlobalPos());
  // }
}
