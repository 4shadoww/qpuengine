#include "game/component/simple_enemy.hpp"
#include "engine/object.hpp"
#include "game/misc/sight.hpp"
#include "engine/globals.hpp"

simple_enemy::simple_enemy(std::string name) : component(name){}
simple_enemy::simple_enemy() : component("simple_enemy"){}
simple_enemy::~simple_enemy(){
  delete simple_enemy::vision;
}

void simple_enemy::init(){
  if(!host->hasCollider()){
    host->createCollider();
    host->collider->setType(physicsbody::t_circle);
  }
  host->addTag("visible");

  simple_enemy::vision = new sight(host);

  simple_enemy::vision->setDebug(true);
}

void simple_enemy::fixedUpdate(){
  simple_enemy::vision->update();
  if(simple_enemy::vision->getSeenObjects().size() > 0){
    engine->debug->info("seen");
  }else{
    engine->debug->info("not seen");
  }
}
