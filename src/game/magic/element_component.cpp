#include "game/magic/element_component.hpp"
#include "engine/globals.hpp"
#include "game/magic/magic_logic.hpp"

element_comp::element_comp(std::string name) : component(name){}
element_comp::element_comp() : component("element_comp"){}

void element_comp::init(){
  host->createCollider();
  host->collider->setType(physicsbody::t_circle);
  host->collider->setRadius(0);
  host->collider->setTrigger(true);
  host->addTag("magic_element");

  host->createCircleMesh();
  host->circlemesh->setRadius(0);
}

void element_comp::addElement(element ele, float count){
  for(std::vector<element_hold>::iterator it = element_comp::elements.begin(); it != element_comp::elements.end(); ++it){
    if(it->m_element.type == ele.type && it->m_element.uneven == ele.uneven && it->m_element.main_negative == ele.main_negative && it->m_element.infinity_negative == ele.infinity_negative){
      it->m_count += count;
      return;
    }
  }
  element_comp::elements.push_back(element_hold(ele, count));
}

void element_comp::addVelocity(float velocity){
  element_comp::velocity.x += velocity;
  element_comp::velocity.y += velocity;
}

void element_comp::addVelocity(float velocity, sf::Vector2f heading){
  heading.x *= velocity;
  heading.y *= velocity;
  element_comp::velocity += heading;
}

void element_comp::updateRadius(){
  float new_radius = 0.f;
  for(std::vector<element_hold>::iterator it = element_comp::elements.begin(); it != element_comp::elements.end(); ++it){
    new_radius += it->m_count;
  }
  new_radius *= element_comp::radius_factor;

  if(new_radius == radius){
    return;
  }

  radius = new_radius;
  host->collider->setRadius(new_radius);
  host->circlemesh->setRadius(new_radius);
  host->circlemesh->setOrigin(new_radius, new_radius);
}

void element_comp::setRadiusFactor(float rf){
  element_comp::radius_factor = rf;
}

float element_comp::getRadiusFactor(){
  return element_comp::radius_factor;
}

void element_comp::drainElements(){
  for(std::vector<element_hold>::iterator it = element_comp::elements.begin(); it != element_comp::elements.end();){
    if(it->m_count <= 0){
      element_comp::elements.erase(it);
      continue;
    }
    it->m_count -= engine->fixedDeltaTime() * 100;
    ++it;
  }
  element_comp::updateRadius();
}

bool element_comp::overValue(float first, float second, float value){
  if(second > value){
    return (second - first < value);
  }
  return (second - first > value);
}

void element_comp::reduceVelocity(){
  float new_velocity;
  if(velocity.x > 0){
    new_velocity = velocity.x - engine->fixedDeltaTime();
    if(overValue(new_velocity, velocity.x, 0.f)){
      velocity.x = 0.f;
    }else{
      velocity.x = new_velocity;
    }
  }else if(velocity.x < 0){
    new_velocity = velocity.x + engine->fixedDeltaTime();
    if(overValue(new_velocity, velocity.x, 0.f)){
      velocity.x = 0.f;
    }else{
      velocity.x = new_velocity;
    }
  }if(velocity.y > 0){
    new_velocity = velocity.y - engine->fixedDeltaTime();
    if(overValue(new_velocity, velocity.y, 0.f)){
      velocity.y = 0.f;
    }else{
      velocity.y = new_velocity;
    }
  }else if(velocity.y < 0){
    new_velocity = velocity.y + engine->fixedDeltaTime();
    if(overValue(new_velocity, velocity.y, 0.f)){
      velocity.y = 0.f;
    }else{
      velocity.y = new_velocity;
    }
  }
}

void element_comp::fixedUpdate(){
  // Drain energy
  if(element_comp::energy > 0){
    element_comp::energy -= engine->fixedDeltaTime();
  // Drain elements if no energy is left
  }else{
    element_comp::drainElements();
  }
  // Destroy if no elements left
  if(element_comp::elements.size() == 0){
    host->destroy();
    return;
  }

  // Update position
  if(velocity.x != 0 || velocity.y != 0){
    host->move(velocity.x * engine->fixedDeltaTime() * 10, velocity.y * engine->fixedDeltaTime() * 10);
  }
  // Reduce velocity
  element_comp::reduceVelocity();

  if(skip_next) return;
  skip_next = true;

  for(std::vector<element_hold>::iterator it0 = element_comp::elements.begin(); it0 != element_comp::elements.end();){
    if(it0->m_count < 0){
      element_comp::elements.erase(it0);
      continue;
    }
    for(std::vector<element_hold>::iterator it1 = element_comp::elements.begin(); it1 != element_comp::elements.end(); ++it1){
      if(it0 == it1) continue;
      if(magic_logic::elementReaction((*it0), (*it1), this)){
        skip_next = false;
        element_comp::updateRadius();
      }
    }
    ++it0;
  }
}
