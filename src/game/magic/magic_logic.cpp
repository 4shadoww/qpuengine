#include "game/magic/magic_logic.hpp"
#include "game/magic/element_component.hpp"
#include "engine/globals.hpp"

func_resolver::func_resolver(int first, int second, bool (*func)(element_hold&, element_hold&)){
  func_resolver::first = first;
  func_resolver::second = second;
  func_resolver::func = func;
}

namespace magic_logic{

element_comp* comp;

std::vector<func_resolver> func_table{
  func_resolver(0, 1, &fireAndFrost),
  func_resolver(0, 2, &fireAndAir),
  func_resolver(0, 3, &fireAndEarth),
  func_resolver(0, 4, &fireAndWater),
  func_resolver(0, 5, &fireAndIce),
  func_resolver(0, 6, &fireAndMist),

  func_resolver(1, 2, &frostAndAir),
  func_resolver(1, 3, &frostAndEarth),
  func_resolver(1, 4, &frostAndWater),
  func_resolver(1, 5, &frostAndIce),
  func_resolver(1, 6, &frostAndMist),

  func_resolver(2, 3, &airAndEarth),
  func_resolver(2, 4, &airAndWater),
  func_resolver(2, 5, &airAndIce),
  func_resolver(2, 6, &airAndMist),

  func_resolver(3, 4, &earthAndWater),
  func_resolver(3, 5, &earthAndIce),
  func_resolver(3, 6, &earthAndMist),

  func_resolver(4, 5, &waterAndIce),
  func_resolver(4, 6, &waterAndMist),

  func_resolver(5, 6, &iceAndMist),
};

// Reactions
bool infinityAny(element_hold& inf, element_hold& norm){
  bool value;
  if(inf.m_element.uneven){
    value = (inf.m_element.main_negative && !norm.m_element.infinity_negative);
    if(value != norm.m_element.infinity_negative){
      norm.m_element.infinity_negative = value;
      return true;
    }
  }else{
    value = (inf.m_element.main_negative && inf.m_element.infinity_negative && norm.m_element.main_negative);
    if(value != norm.m_element.main_negative){
      norm.m_element.main_negative = value;
      if(!norm.m_element.uneven) norm.m_element.infinity_negative = value;
      return true;
    }
  }
  return false;
}

bool fireAndFrost(element_hold& fire, element_hold& frost){
  fire.m_count -= engine->fixedDeltaTime();
  frost.m_count -= engine->fixedDeltaTime();
  return true;
}

bool fireAndAir(element_hold& fire, element_hold& air){
  if(fire.m_count / air.m_count <= 0.10f){
    return true;
  }
  fire.m_count -= engine->fixedDeltaTime() * (1.f + fire.m_count / air.m_count);
  return true;
}

bool fireAndEarth(element_hold& fire, element_hold& earth){
  if(fire.m_count / earth.m_count <= 0.1){
    earth.m_count -= engine->fixedDeltaTime();
    return true;
  }
  return false;
}

bool fireAndWater(element_hold& fire, element_hold& water){
  fire.m_count -= engine->fixedDeltaTime() * 1.1f;
  water.m_count -= engine->fixedDeltaTime();
  comp->addElement(element(element::mist, false, false), engine->fixedDeltaTime());
  return true;
}

bool fireAndIce(element_hold& fire, element_hold& ice){
  fire.m_count -= engine->fixedDeltaTime();
  ice.m_count -= engine->fixedDeltaTime() * 2.f;
  comp->addElement(element(element::water, false, false), engine->fixedDeltaTime() * 2.f);
  return true;
}

bool fireAndMist(element_hold& fire, element_hold& mist){
  return false;
}

bool frostAndAir(element_hold& frost, element_hold& air){
  return false;
}

bool frostAndEarth(element_hold& frost, element_hold& earth){
  return false;
}

bool frostAndWater(element_hold& frost, element_hold& water){
  frost.m_count -= engine->fixedDeltaTime();
  water.m_count -= engine->fixedDeltaTime();
  comp->addElement(element(element::ice, false, false), engine->fixedDeltaTime());
  return true;
}

bool frostAndIce(element_hold& frost, element_hold& ice){
  frost.m_count -= engine->fixedDeltaTime();
  ice.m_count += engine->fixedDeltaTime();
  return true;
}

bool frostAndMist(element_hold& frost, element_hold& mist){
  return false;
}

bool airAndEarth(element_hold& air, element_hold& earth){
  return false;
}

bool airAndWater(element_hold& air, element_hold& water){
  return false;
}

bool airAndIce(element_hold& air, element_hold& ice){
  return false;
}

bool airAndMist(element_hold& air, element_hold& mist){
  return false;
}

bool earthAndWater(element_hold& earth, element_hold& water){
  return false;
}

bool earthAndIce(element_hold& earth, element_hold& ice){
  return false;
}

bool earthAndMist(element_hold& earth, element_hold& mist){
  return false;
}

bool waterAndIce(element_hold& water, element_hold& ice){
  water.m_count -= engine->fixedDeltaTime();
  ice.m_count += engine->fixedDeltaTime();
  return true;
}

bool waterAndMist(element_hold& water, element_hold& mist){
  water.m_count += engine->fixedDeltaTime() * 5;
  mist.m_count -= engine->fixedDeltaTime() * 5;
  return true;
}

bool iceAndMist(element_hold& ice, element_hold& mist){
  return false;
}
// Reactions end

bool elementReaction(element_hold& ele0, element_hold& ele1, element_comp* ele_fus){
  comp = ele_fus;

  if(ele0.m_count <= 0 || ele1.m_count <= 0) return false;

  int first = ele0.m_element.type;
  int second = ele1.m_element.type;
  // Infinity
  if(first == element::infinity && second != element::infinity){
    return magic_logic::infinityAny(ele0, ele1);
  }else if(second == element::infinity && first != element::infinity){
    return magic_logic::infinityAny(ele1, ele0);
  }

  for(std::vector<func_resolver>::iterator it = func_table.begin(); it != func_table.end(); ++it){
    if(it->first == first && it->second == second){
      return it->func(ele0, ele1);
    }else if(it->first == second && it->second == first){
      return it->func(ele1, ele0);
    }
  }
  return false;
}
};
