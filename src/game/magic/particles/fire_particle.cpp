#include "game/magic/particles/fire_particle.hpp"

fire_particle::fire_particle(){
  fire_particle::useGlobalSpace(true);
  fire_particle::setVelocity(3.f);
  fire_particle::setParticleCount(150);
  fire_particle::setSpawnRadius(10.f);
  fire_particle::setColors(sf::Color::Yellow, sf::Color::Red);
  fire_particle::setLifeTimes(0.1f, 1.f);
}
