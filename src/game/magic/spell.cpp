#include "game/magic/spell.hpp"

spell::node::node(node_type action_type, element::etype type, float count, bool uneven, bool negative, float velocity, float energy){
  spell::node::action_type = action_type;
  spell::node::type = type;
  spell::node::count = count;
  spell::node::uneven = uneven;
  spell::node::negative = negative;
  spell::node::velocity = velocity;
  spell::node::energy = energy;
}

spell::node::node(node_type type, float sleep){
  spell::node::action_type = action_type;
  spell::node::sleep = sleep;
}
