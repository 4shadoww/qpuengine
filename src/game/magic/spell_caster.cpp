#include "engine/globals.hpp"
#include "game/magic/spell_caster.hpp"
#include "game/magic/element_component.hpp"
#include "engine/math.hpp"

spell_caster::spell_caster(std::string name) : component(name){}
spell_caster::spell_caster() : component("spell_caster"){}

void spell_caster::init(){
  spell_caster::current_pos = 0;
}

void spell_caster::castSpell(spell* mag_spell){
  spell_caster::castSpell(mag_spell, qpumath::getAngle(host->getPosition() - engine->getMouseGlobalPos()));
}

void spell_caster::castSpell(spell* mag_spell, float angle){
  spell_caster::spell_to_cast = mag_spell;
  spell_caster::casting = true;
  spell_caster::angle = angle;
  spell_caster::heading = qpumath::angleToVector(angle);
}

void spell_caster::setDistance(float distance){
  spell_caster::distance = distance;
}

float spell_caster::getDistance(){
  return spell_caster::distance;
}

void spell_caster::createElement(spell::node& node, element_comp* elfus){
  elfus->addElement(element(node.type, node.uneven, node.negative), node.count);
  elfus->addVelocity(node.velocity, spell_caster::heading);
}

void spell_caster::fixedUpdate(){
  object* ob;
  element_comp* elfus;

  if(!spell_caster::casting) return;

  if(spell_caster::current_pos == spell_caster::spell_to_cast->instructions.size()){
    spell_caster::casting = false;
    spell_caster::current_pos = 0;
    return;
  }

  if(spell_caster::sleeping && spell_caster::spell_to_cast->instructions.at(spell_caster::current_pos).at(0).sleep <= spell_caster::slept){
    spell_caster::sleeping = false;
    spell_caster::current_pos++;
  }

  // Sleep
  if(sleeping){
    spell_caster::slept += engine->fixedDeltaTime();
    return;
  }

  // Cast elements
  if(spell_to_cast->instructions.at(current_pos).at(0).action_type == spell::node::create){
    ob = engine->createObject("magic");
    elfus = ob->addComponent(new element_comp);
  }
  for(std::vector<spell::node>::iterator it = spell_to_cast->instructions.at(current_pos).begin(); it != spell_to_cast->instructions.at(current_pos).end(); ++it){
    if(it->action_type == spell::node::create){
      spell_caster::createElement((*it), elfus);
    }else if(it->action_type == spell::node::wait){
      spell_caster::sleeping = true;
      spell_caster::slept = 0.f;
      return;
    }
  }
  elfus->updateRadius();
  // Set position
  ob->setPosition(qpumath::getEndPoint(host->getPosition(), angle, distance));
  current_pos++;
}
