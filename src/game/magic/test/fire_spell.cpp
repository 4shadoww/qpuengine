#include "game/magic/test/fire_spell.hpp"

#include <vector>

fire_spell::fire_spell(){
  spell::instructions.push_back(std::vector<node>{
    node(node::create, element::fire, 100, false, false, 20.f),
    //node(node::create, element::infinity, true, true),

  });
}
