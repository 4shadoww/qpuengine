#include <fstream>

#include "game/maploader.hpp"
#include "engine/object.hpp"
#include "engine/globals.hpp"
#include "engine/component/physicsbody.hpp"
#include "game/component/mapvertex.hpp"
#include "engine/custommesh.hpp"

maploader::maploader(){
  maploader::spriteload.loadSprites(engine->getPath()+"sprites/sprite.png");
}

void maploader::loadConfig(Json::Value &json){
  maploader::width = json["width"].asInt();
  maploader::height = json["height"].asInt();
  maploader::tilewidth = json["tilewidth"].asInt();
}

object* maploader::createPolygon(Json::Value &data){
  int i = 0;
  object* ob = engine->objmgr->createObject("ml_collider");
  ob->createCollider();
  ob->collider->setStatic(true);
  ob->collider->setVerticleCount(data["polygon"].size());
  ob->setPosition(data["x"].asFloat() + maploader::offsetx, data["y"].asFloat() + maploader::offsety);
  for(Json::Value::const_iterator it = data["polygon"].begin(); it != data["polygon"].end(); ++it){
    ob->collider->setVecticlePosition(i, (*it)["x"].asFloat(), (*it)["y"].asFloat());
    i += 1;
  }
  return ob;
}

object* maploader::createEllipse(Json::Value &data){
  object* ob = engine->objmgr->createObject("ml_collider");
  ob->createCollider();
  ob->collider->setStatic(true);
  ob->collider->setType(physicsbody::t_circle);
  ob->collider->setRadius(data["width"].asFloat() / 2);
  ob->setPosition(data["x"].asFloat() +  maploader::offsetx + data["width"].asFloat() / 2, data["y"].asFloat() + maploader::offsety + data["width"].asFloat() / 2);
  return ob;
}

object* maploader::createCube(Json::Value &data){
  object* ob = engine->objmgr->createObject("ml_collider");
  ob->createCollider();
  ob->collider->setStatic(true);
  ob->collider->setVerticleCount(4);
  ob->setPosition(data["x"].asFloat() + maploader::offsetx - maploader::tilewidth / 2, data["y"].asFloat() + maploader::offsety - maploader::tilewidth / 2);
  ob->collider->setVecticlePosition(0, 0, 0);
  ob->collider->setVecticlePosition(1, data["width"].asFloat(), 0);
  ob->collider->setVecticlePosition(2, data["width"].asFloat(), data["height"].asFloat());
  ob->collider->setVecticlePosition(3, 0, data["height"].asFloat());
  return ob;
}

void maploader::loadPhysics(Json::Value &data){
  object* ob;
  for(Json::Value::iterator it = data["objects"].begin(); it != data["objects"].end(); ++it){
    if((*it).isMember("polygon")){
      ob = maploader::createPolygon((*it));
      ob->addParent(maploader::maproot);
    }else if((*it).isMember("ellipse") && (*it)["ellipse"] == true){
      ob = maploader::createEllipse((*it));
      ob->addParent(maploader::maproot);
    }else{
      ob = maploader::createCube((*it));
      ob->addParent(maploader::maproot);
    }
  }
}

bool maploader::loadMap(std::string mapjson, int x, int y){
  maploader::maproot = engine->objmgr->createObject("map_root");
  object* mapob;
  mapvertex* mapver;
  int i = 0;

  Json::Value root;
  try{
    std::ifstream map_data(engine->getPath()+mapjson, std::ifstream::binary);
    map_data >> root;
  }catch(Json::RuntimeError){
    return false;
  }
  sf::Texture* texture = new sf::Texture;
  texture->loadFromFile(engine->getPath()+"sprites/sprite.png");

  maploader::offsetx = x;
  maploader::offsety = y;
  maploader::loadConfig(root);
  for(Json::Value::iterator it = root["layers"].begin(); it != root["layers"].end(); it++){
    if((*it)["type"] == "tilelayer"){
      mapver = new mapvertex;
      mapver->load(texture, maploader::tilewidth, (*it)["data"], maploader::width, maploader::height, sf::Vector2f(x, y));
      mapob = engine->objmgr->createObject("ml_map_object");
      mapob->setLayer(i);
      mapob->setPosition(offsetx - maploader::tilewidth / 2, offsety - maploader::tilewidth / 2);
      mapob->setCustomMesh(static_cast<custommesh*>(mapver));
      mapob->addParent(maploader::maproot);
      i++;
    }else if((*it)["type"] == "objectgroup" && (*it)["name"] == "collider"){
      maploader::loadPhysics((*it));
    }
  }
  return true;
}

// Sprite loader
// Load sprite
bool spriteloader::loadSprites(std::string name){
  if(!spriteloader::spritesrc.loadFromFile(name)) return false;
  spriteloader::generateSprites();
  return true;
}

void spriteloader::generateSprites(){
  sf::Texture* texture;
  int x = spriteloader::spritesrc.getSize().x / spriteloader::width;
  int y = spriteloader::spritesrc.getSize().y / spriteloader::height;
  spriteloader::sprites.clear();

  for(int i = 0; i < y; i++){
    for(int ii = 0; ii < x; ii++){
      texture = new sf::Texture;
      texture->setSmooth(false);
      texture->loadFromImage(spriteloader::spritesrc, sf::IntRect(ii * spriteloader::width, i * spriteloader::height, spriteloader::width, spriteloader::height));
      spriteloader::sprites.push_back(texture);
    }
  }
}

void spriteloader::setSpriteSize(int width, int height){
  spriteloader::width = width;
  spriteloader::height = height;
}

sf::Texture* spriteloader::getSprite(int id){
  id -= 1;
  return spriteloader::sprites.at(id);
}
