#include "game/misc/sight.hpp"
#include "engine/globals.hpp"
#include "engine/math.hpp"
#include "engine/object.hpp"
#include "engine/component/raycast.hpp"
#include "engine/customvertex/line.hpp"

sight::sight(object* host){
  sight::host = host;
  sight::reCalculateMinMax();
  ray = host->addComponent(new raycast);
  ray->setMagnitude(sight::distance);
}

sight::~sight(){
  if(sight::debug){
    sight::setDebug(false);
  }
  host->removeComponent(ray);
}

void sight::update(){
  std::vector<object*> objects = engine->objmgr->getObjects();

  bool seen;

  sight::objects.clear();

  for(std::vector<object*>::iterator it = objects.begin(); it != objects.end(); ++it){
    if((*it) == host) continue;
    if((*it)->destroyed() || !(*it)->enabled()) continue;
    if(!(*it)->hasTag("visible") || !(*it)->hasCollider()) continue;

    seen = false;

    switch((*it)->collider->getType()){
      case physicsbody::t_convex:
        seen = sight::testConvex((*it));
        break;
      case physicsbody::t_circle:
        seen = sight::testCircle((*it));
        break;
    }
    if(!seen){
      continue;
    }
    sight::objects.push_back((*it));
  }
}

bool sight::convexInSector(object* ob){
  sf::Vector2f min;
  sf::Vector2f max;

  sf::Vector2f ob_pos = ob->getPosition();
  sf::Vector2f host_pos = host->getPosition();

  // Check is object in the sector (not accurate)
  float angle = qpumath::getAngle(ob_pos - host_pos);
  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  min = ob->collider->getMin();
  angle = qpumath::getAngle(ob_pos + min - host_pos);

  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  min.y = -min.y;
  angle = qpumath::getAngle(ob_pos + min - host_pos);

  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  max = ob->collider->getMax();
  angle = qpumath::getAngle(ob_pos + max - host_pos);

  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  max.y = -max.y;
  angle = qpumath::getAngle(ob_pos + max - host_pos);

  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  return false;
}

bool sight::circleInSector(object* ob){
  float radius = ob->collider->getRadius();
  float temp;
  sf::Vector2f ob_pos = ob->getPosition();
  sf::Vector2f host_pos = host->getPosition();
  sf::Vector2f point(radius / 2, radius);

  float angle = qpumath::getAngle(ob_pos - host_pos);
  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  angle = qpumath::getAngle(ob_pos + point - host_pos);
  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  point.y = -point.y;
  angle = qpumath::getAngle(ob_pos + point - host_pos);
  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  temp = point.x;
  point.x = point.y;
  point.y = temp;
  angle = qpumath::getAngle(ob_pos + point - host_pos);
  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  point.x = -point.x;
  angle = qpumath::getAngle(ob_pos + point - host_pos);
  if(qpumath::inSector(angle, sight::fov_min, sight::fov_max)) return true;

  return false;
}

bool sight::testConvex(object* ob){
  if(!sight::convexInSector(ob)) return false;

  sf::Vector2f min = ob->collider->getMin();
  sf::Vector2f max = ob->collider->getMax();

  sf::Vector2f ob_pos = ob->getPosition();

  sf::Vector2f glob_min0 = min + ob_pos;
  sf::Vector2f glob_max0 = max + ob_pos;

  min.x = -min.x;
  max.x = -max.x;

  sf::Vector2f glob_min1 = min + ob_pos;
  sf::Vector2f glob_max1 = max + ob_pos;

  sf::Vector2f host_pos = host->getPosition();

  float min_angle = qpumath::getAngle(glob_min0 - host_pos);
  float max_angle = qpumath::getAngle(glob_max0 - host_pos);

  float min_angle_alt = qpumath::getAngle(glob_min1 - host_pos);
  float max_angle_alt = qpumath::getAngle(glob_max1 - host_pos);

  float angle_diff = qpumath::getMinAngleDiff(min_angle, max_angle);
  float angle_diff_alt = qpumath::getMinAngleDiff(min_angle_alt, max_angle_alt);

  if(angle_diff < angle_diff_alt){
    angle_diff = angle_diff_alt;
    min_angle = min_angle_alt;
    max_angle = max_angle_alt;
  }

  float delta = angle_diff / sight::raycast_count;

  for(unsigned int i = 0; i < sight::raycast_count; i++){
    ray->setAngle(qpumath::fixedAngle(min_angle + i * delta));
    ray->updateRaycast();
    if(ray->getHittingBody() == ob->collider) return true;
  }

  return false;
}

bool sight::testCircle(object* ob){
  if(!sight::circleInSector(ob)) return false;

  sf::Vector2f min = ob->collider->getMin();
  sf::Vector2f max = ob->collider->getMax();

  sf::Vector2f ob_pos = ob->getPosition();

  sf::Vector2f glob_min0 = min + ob_pos;
  sf::Vector2f glob_max0 = max + ob_pos;

  min.x = -min.x;
  max.x = -max.x;

  sf::Vector2f glob_min1 = min + ob_pos;
  sf::Vector2f glob_max1 = max + ob_pos;

  sf::Vector2f host_pos = host->getPosition();

  float min_angle = qpumath::getAngle(glob_min0 - host_pos);
  float max_angle = qpumath::getAngle(glob_max0 - host_pos);

  float min_angle_alt = qpumath::getAngle(glob_min1 - host_pos);
  float max_angle_alt = qpumath::getAngle(glob_max1 - host_pos);

  float angle_diff = qpumath::getMinAngleDiff(min_angle, max_angle);
  float angle_diff_alt = qpumath::getMinAngleDiff(min_angle_alt, max_angle_alt);

  if(angle_diff < angle_diff_alt){
    angle_diff = angle_diff_alt;
    min_angle = min_angle_alt;
    max_angle = max_angle_alt;
  }

  float delta = angle_diff / sight::raycast_count;

  for(unsigned int i = 0; i < sight::raycast_count; i++){
    ray->setAngle(qpumath::fixedAngle(min_angle + i * delta));
    ray->updateRaycast();
    if(ray->getHittingBody() == ob->collider) return true;
  }

  return false;
}

void sight::reCalculateMinMax(){
  sight::fov_min = qpumath::fixedAngle(sight::facing_angle - sight::fov / 2);
  sight::fov_max = qpumath::fixedAngle(sight::facing_angle + sight::fov / 2);
}

void sight::setFov(float fov){
  sight::fov = fov;
  sight::reCalculateMinMax();
}

float sight::getFov(){
  return sight::fov;
}

void sight::setFacingAngle(float angle){
  sight::facing_angle = angle;
}

float sight::getFacingAngle(){
  return sight::facing_angle;
}

void sight::setDebug(bool value){
  if(value == sight::debug) return;
  sight::debug = value;

  if(sight::debug){
    sight::min_ob = engine->createObject("_debug_line");
    sight::max_ob = engine->createObject("_debug_line");
    sight::min_line = min_ob->setCustomMesh(new linevertex);
    sight::max_line = max_ob->setCustomMesh(new linevertex);
    sight::min_line->setPositions(host->getPosition(), qpumath::getEndPoint(host->getPosition(), sight::fov_min, sight::distance));
    sight::max_line->setPositions(host->getPosition(), qpumath::getEndPoint(host->getPosition(), sight::fov_max, sight::distance));
  }else{
    sight::min_ob->destroy();
    sight::max_ob->destroy();
  }
}

bool sight::isDebugEnabled(){
  return sight::debug;
}

void sight::updateDebugDraw(){
  sight::min_line->setPositions(host->getPosition(), qpumath::getEndPoint(host->getPosition(), sight::fov_min, sight::distance));
  sight::max_line->setPositions(host->getPosition(), qpumath::getEndPoint(host->getPosition(), sight::fov_max, sight::distance));
}

std::vector<object*>& sight::getSeenObjects(){
  return sight::objects;
}
