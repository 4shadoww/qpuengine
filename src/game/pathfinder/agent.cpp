#include <cmath>

#include "game/pathfinder/agent.hpp"
#include "game/pathfinder/enviroment.hpp"
#include "game/pathfinder/node.hpp"
#include "engine/globals.hpp"

pathfinder::pathfinder(pathfinden* enviroment){
  pathfinder::setEnviroment(enviroment);
}

pathfinder::pathfinder(){}

pathfinder::~pathfinder(){
  pathfinder::clearLists();
}

void pathfinder::setEnviroment(pathfinden* enviroment){
  pathfinder::enviroment = enviroment;
  pathfinder::width = pathfinder::enviroment->getWidth();
  pathfinder::height = pathfinder::enviroment->getHeight();
  pathfinder::node_width = pathfinder::enviroment->getNodeWidth();
  pathfinder::node_height = pathfinder::enviroment->getNodeHeight();
}

sf::Vector2i pathfinder::getClosestNode(float x, float y){
  return pathfinder::getClosestNode(sf::Vector2f(x, y));
}

sf::Vector2i pathfinder::getClosestNode(sf::Vector2f position){
  position.x += pathfinder::node_width / 2;
  position.y += pathfinder::node_height / 2;
  int x, y;
  if(position.x == 0){
    x = 0;
  }else{
    x = static_cast<int>((position.x + 0.5f) / pathfinder::node_width);
  }
  if(position.y == 0){
    y = 0;
  }else{
    y = static_cast<int>((position.y + 0.5f) / pathfinder::node_height);
  }
  return sf::Vector2i(x, y);
}

int pathfinder::getHeuristic(pathnode* start, pathnode* end){
  return fabsf(start->x - end->x) + fabsf(start->y - end->y);
}

int pathfinder::getMovementCost(pathnode* node){
  if(node->parent){
    if(node->x != node->parent->x && node->y != node->parent->y){
      return node->parent->g + 14;
    }else{
      return node->parent->g + 10;
    }
  }
  return 0;
}

void pathfinder::calculateValues(pathnode* node){
  node->h = pathfinder::getHeuristic(node, pathfinder::end);
  node->g = pathfinder::getMovementCost(node);
  node->f = node->h + node->g;
}

void pathfinder::findNodes(pathnode* position){
  int pos_x, pos_y;
  pathnode* cu_pos;
  for(int y = -1; y < 2; y++){
    for(int x = -1; x < 2; x++){
      pos_x = position->x + x;
      pos_y = position->y + y;

      if(pos_x == position->x && pos_y == position->y){
        continue;
      }else if(!pathfinder::inRange(pos_x, pos_y) || pathfinder::enviroment->ignoredNode(pos_x, pos_y)){
        continue;
      }else if(pathfinder::inClosedList(pos_x, pos_y)){
        continue;
      }else if(pathfinder::inOpenList(pos_x, pos_y)){
        cu_pos = pathfinder::getOpenNode(pos_x, pos_y);
        if(pathfinder::shouldReparent(position, cu_pos) && !pathfinder::enviroment->ignoredMovement(position->x, position->y, cu_pos->x, cu_pos->y)){
          cu_pos->parent = position;
          pathfinder::calculateValues(cu_pos);
        }
      }else if(!pathfinder::enviroment->ignoredMovement(position->x, position->y, pos_x, pos_y)){
        cu_pos = new pathnode(pos_x, pos_y);
        cu_pos->parent = position;
        pathfinder::calculateValues(cu_pos);
        pathfinder::open_list.push_back(cu_pos);
      }
    }
  }
}

pathnode* pathfinder::updateClosed(){
  pathnode* lowest = (*pathfinder::open_list.begin());
  std::vector<pathnode*>::iterator removeindex = pathfinder::open_list.begin();
  for(std::vector<pathnode*>::iterator it = pathfinder::open_list.begin(); it != pathfinder::open_list.end(); ++it){
    if(lowest->f > (*it)->f){
      lowest = (*it);
      removeindex = it;
    }
  }
  pathfinder::open_list.erase(removeindex);
  pathfinder::closed_list.push_back(lowest);
  return lowest;
}

void pathfinder::findPath(pathnode* end){
  pathnode* current = end;
  while(current->parent){
    pathfinder::path.insert(pathfinder::path.begin(), current);
    current = current->parent;
  }
}

bool pathfinder::calculatePath(pathnode start, pathnode end){
  if(start.x == end.x && start.y == end.y) return true;
  if(!pathfinder::enviroment) return false;
  if(!pathfinder::inRange(start) || !pathfinder::inRange(end)) return false;
  if(pathfinder::enviroment->ignoredNode(end.x, end.y)) return false;
  // Clear lists
  pathfinder::clearLists();

  pathfinder::end = new pathnode(end.x, end.y);

  pathnode* cu_pos = new pathnode(start.x, start.y);
  pathfinder::calculateValues(cu_pos);
  pathfinder::closed_list.push_back(cu_pos);

  pathfinder::start = cu_pos;

  while(true){
    pathfinder::findNodes(cu_pos);
    if(pathfinder::open_list.size() == 0) return false;
    cu_pos = pathfinder::updateClosed();
    if(pathfinder::getHeuristic(cu_pos, pathfinder::end) < 2){
      pathfinder::end->parent = cu_pos;
      pathfinder::findPath(pathfinder::end);
      return true;
    }
  }
}

bool pathfinder::calculatePath(int start_x, int start_y, int end_x, int end_y){
  return pathfinder::calculatePath(pathnode(start_x, start_y), pathnode(end_x, end_y));
}

bool pathfinder::calculatePath(sf::Vector2i start, sf::Vector2i end){
  return pathfinder::calculatePath(pathnode(start.x, start.y), pathnode(end.x, end.y));
}

// Should reparent
bool pathfinder::shouldReparent(pathnode* node0, pathnode* node1){
  int movement_cost;

  if(node1->x != node0->x && node1->y != node0->y){
    movement_cost = 14;
  }else{
    movement_cost = 10;
  }

  return (node1->f > node0->f + movement_cost + node1->h);
}

// Clear lists
void pathfinder::clearLists(){
  for(std::vector<pathnode*>::iterator it = pathfinder::open_list.begin(); it != pathfinder::open_list.end(); ++it){
    delete (*it);
  }
  pathfinder::open_list.clear();
  for(std::vector<pathnode*>::iterator it = pathfinder::closed_list.begin(); it != pathfinder::closed_list.end(); ++it){
    delete (*it);
  }
  pathfinder::closed_list.clear();
  pathfinder::path.clear();
  delete pathfinder::end;
  pathfinder::end = nullptr;
}

// In list
bool pathfinder::inOpenList(int x, int y){
  for(std::vector<pathnode*>::iterator it = pathfinder::open_list.begin(); it != pathfinder::open_list.end(); ++it){
    if((*it)->x == x && (*it)->y == y) return true;
  }
  return false;
}

bool pathfinder::inClosedList(int x, int y){
  for(std::vector<pathnode*>::iterator it = pathfinder::closed_list.begin(); it != pathfinder::closed_list.end(); ++it){
    if((*it)->x == x && (*it)->y == y) return true;
  }
  return false;
}

// Get from open list
pathnode* pathfinder::getOpenNode(int x, int y){
  for(std::vector<pathnode*>::iterator it = pathfinder::open_list.begin(); it != pathfinder::open_list.end(); ++it){
    if((*it)->x == x && (*it)->y == y) return (*it);
  }
  return nullptr;
}

// Get from closed list
pathnode* pathfinder::getClosedNode(int x, int y){
  for(std::vector<pathnode*>::iterator it = pathfinder::closed_list.begin(); it != pathfinder::closed_list.end(); ++it){
    if((*it)->x == x && (*it)->y == y) return (*it);
  }
  return nullptr;
}

// Position is in the navigation space
bool pathfinder::inRange(pathnode position){
  return (position.x >= 0 && position.x <= pathfinder::width && position.y >= 0 && position.y <= pathfinder::height);
}

bool pathfinder::inRange(int x, int y){
  return pathfinder::inRange(pathnode(x, y));
}

bool pathfinder::inRange(sf::Vector2f position){
  return pathfinder::inRange(pathnode(position.x, position.y));
}

bool pathfinder::inRange(pathnode* position){
  return pathfinder::inRange((*position));
}

bool pathfinder::xInRange(int x){
  return (x >= 0 && x <= pathfinder::width);
}

bool pathfinder::yInRange(int y){
  return (y >= 0 && y <= pathfinder::height);
}

// Collider methods
void pathfinder::setCollider(object* ob){
  pathfinder::removeCollider();
  pathfinder::testob = engine->createObject("agent_test_collider_");
  pathfinder::testob->createCollider();
  if(ob->collider->getType() == physicsbody::t_convex){
    pathfinder::testob->collider->setVerticles(ob->collider->getVerticles());
  }else if(ob->collider->getType() == physicsbody::t_circle){
    pathfinder::testob->collider->setType(physicsbody::t_circle);
    pathfinder::testob->collider->setRadius(ob->collider->getRadius());
  }
  pathfinder::testob->collider->setTrigger(true);
  pathfinder::testob->setEnabled(false);
  pathfinder::testob->setPosition(100, 100);
  pathfinder::enable_collision = true;
}

object* pathfinder::getCollider(){
  return pathfinder::testob;
}

void pathfinder::removeCollider(){
  if(pathfinder::testob){
    pathfinder::testob->destroy();
    pathfinder::testob = nullptr;
  }
}

bool pathfinder::isSafe(int x0, int y0, int x1, int y1, bool ig_hv){
  if(!pathfinder::enable_collision) return true;
  if(!pathfinder::testob) return false;
  if((ig_hv && x0 == x1) || (ig_hv && y0 == y1)) return true;
  float pos_x = (x0 * pathfinder::node_width + x1 * pathfinder::node_width) / 2;
  float pos_y = (y0 * pathfinder::node_height + y1 * pathfinder::node_height) / 2;

  pathfinder::testob->setEnabled(true);
  pathfinder::testob->setPosition(pos_x, pos_y);

  if(engine->phyen->getGap(pathfinder::testob, false) >= 0.f){
    pathfinder::testob->setEnabled(false);
    return true;
  }
  pathfinder::testob->setEnabled(false);
  return false;
}

sf::Vector2f pathfinder::getNodePosition(int i){
  pathnode* node = pathfinder::path.at(i);
  return sf::Vector2f(node->x * pathfinder::node_width, node->y * pathfinder::node_height);
}

void pathfinder::enableCollision(bool value){
  pathfinder::enable_collision = value;
}

bool pathfinder::collisionEnabled(){
  return pathfinder::enable_collision;
}

void pathfinder::debug(){
  object* ob;
  for(std::vector<pathnode*>::iterator it = pathfinder::path.begin(); it != pathfinder::path.end(); ++it){
    ob = engine->createObject("pathf_debug_");
    ob->createCircleMesh();
    ob->circlemesh->setRadius(6);
    ob->circlemesh->setOrigin(6, 6);
    ob->setPosition((*it)->x * pathfinder::node_width, (*it)->y * pathfinder::node_height);
  }
}
