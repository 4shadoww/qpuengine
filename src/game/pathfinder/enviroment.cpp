#include "game/pathfinder/enviroment.hpp"
#include "game/pathfinder/node.hpp"
#include "engine/globals.hpp"
#include "engine/object.hpp"
#include "engine/component/physicsbody.hpp"

pathfinden::pathfinden(){}

pathfinden::pathfinden(int width, int height, int node_width, int node_height){
  pathfinden::width = width;
  pathfinden::height = height;
  pathfinden::node_width = node_width;
  pathfinden::node_height = node_height;
}

int pathfinden::getWidth(){
  return pathfinden::width;
}

int pathfinden::getHeight(){
  return pathfinden::height;
}

int pathfinden::getNodeWidth(){
  return pathfinden::node_width;
}

int pathfinden::getNodeHeight(){
  return pathfinden::node_width;
}

void pathfinden::generateUnsafeNodes(){
  // Clear nodes
  pathfinden::nodes.clear();
  // Create test node
  object* ob;
  ob = engine->objmgr->createObject("pf_tn_");
  ob->createCollider();
  ob->collider->setVecticlePosition(0, -(pathfinden::node_width) / 2, (pathfinden::node_height) / 2);
  ob->collider->setVecticlePosition(1, (pathfinden::node_width) / 2, (pathfinden::node_height) / 2);
  ob->collider->setVecticlePosition(2, (pathfinden::node_width) / 2, -(pathfinden::node_height) / 2);
  ob->collider->setVecticlePosition(3, -(pathfinden::node_width) / 2, -(pathfinden::node_height) / 2);
  ob->collider->setTrigger(true);

  // Loop over every node
  for(int y = 0; y < pathfinden::height; y++){
    for(int x = 0; x < pathfinden::width; x++){
      ob->setPosition(x * pathfinden::node_width, y * pathfinden::node_height);
      if(engine->phyen->getGap(ob, false) < 0.f){
        pathfinden::nodes.push_back(nodevec(x, y));
      }
    }
  }
  ob->destroy();
}

void pathfinden::generateUnsafeMovements(){
  int pos_x, pos_y;
  // Clear nodes
  pathfinden::movements.clear();
  pathfinden::createTestCollider();
  for(int y = 0; y < pathfinden::height; y++){
    for(int x = 0; x < pathfinden::width; x++){
      if(pathfinden::ignoredNode(x, y)) continue;
      for(int y0 = -1; y0 < 2; y0 += 2){
        for(int x0 = -1; x0 < 2; x0 += 2){
          pos_x = x + x0;
          pos_y = y + y0;
          if(!pathfinden::inRange(pos_x, pos_y)) continue;
          if(pathfinden::ignoredNode(pos_x, pos_y)) continue;
          if(pathfinden::ignoredMovement(pos_x, pos_y, x, y)) continue;
          if(!pathfinden::isSafe(pos_x, pos_y, x, y)){
            pathfinden::movements.push_back(movement(pos_x, pos_y, x, y));
          }
        }
      }
    }
  }
  pathfinden::testob->destroy();
  pathfinden::testob = nullptr;
}

void pathfinden::generateEnviroment(){
  pathfinden::generateUnsafeNodes();
  pathfinden::generateUnsafeMovements();
}

std::vector<nodevec> &pathfinden::getNodes(){
  return pathfinden::nodes;
}

bool pathfinden::ignoredNode(int x, int y){
  for(std::vector<nodevec>::iterator it = pathfinden::nodes.begin(); it != pathfinden::nodes.end(); ++it){
    if(x == it->x && y == it->y) return true;
  }
  return false;
}

bool pathfinden::ignoredMovement(int x0, int y0, int x1, int y1){
  for(std::vector<movement>::iterator it = pathfinden::movements.begin(); it != pathfinden::movements.end(); ++it){
    if(it->x0 == x0 && it->y0 == y0 && it->x1 == x1 && it->y1 == y1){
      return true;
    }
    if(it->x1 == x0 && it->y1 == y0 && it->x0 == x1 && it->y0 == y1){
      return true;
    }
  }
  return false;
}

bool pathfinden::inRange(int x, int y){
  return (x >= 0 && x <= pathfinden::width && y >= 0 && y <= pathfinden::height);
}

bool pathfinden::isSafe(int x0, int y0, int x1, int y1, bool ig_hv){
  if((ig_hv && x0 == x1) || (ig_hv && y0 == y1)) return true;
  float pos_x = (x0 * pathfinden::node_width + x1 * pathfinden::node_width) / 2;
  float pos_y = (y0 * pathfinden::node_height + y1 * pathfinden::node_height) / 2;

  pathfinden::testob->setPosition(pos_x, pos_y);

  if(engine->phyen->getGap(pathfinden::testob, false) >= 0.f){
    return true;
  }
  return false;
}

void pathfinden::createTestCollider(){
  testob = engine->createObject("testob");
  testob->createCollider();
  testob->collider->setType(physicsbody::t_circle);
  testob->collider->setRadius(node_width / 2);
  testob->collider->setTrigger(true);
}

void pathfinden::debug(){
  object* ob;
  for(std::vector<nodevec>::iterator it = pathfinden::nodes.begin(); it != pathfinden::nodes.end(); ++it){
    ob = engine->objmgr->createObject("debug");
    ob->setPosition(it->x * pathfinden::node_width, it->y * pathfinden::node_height);
    ob->createConvexMesh();
    ob->convexmesh->setPointCount(4);
    ob->convexmesh->setPoint(0, sf::Vector2f(pathfinden::node_width / 2, pathfinden::node_height / 2));
    ob->convexmesh->setPoint(1, sf::Vector2f(-pathfinden::node_width / 2, pathfinden::node_height / 2));
    ob->convexmesh->setPoint(2, sf::Vector2f(-pathfinden::node_width / 2, -pathfinden::node_height / 2));
    ob->convexmesh->setPoint(3, sf::Vector2f(pathfinden::node_width / 2, -pathfinden::node_height / 2));
  }
}
