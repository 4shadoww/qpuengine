#include "game/pathfinder/node.hpp"

nodevec::nodevec(){}

nodevec::nodevec(int x, int y){
  nodevec::x = x;
  nodevec::y = y;
}

pathnode::pathnode(){}

pathnode::pathnode(int x, int y){
  pathnode::x = x;
  pathnode::y = y;
}

movement::movement(){}

movement::movement(int x0, int y0, int x1, int y1){
  movement::x0 = x0;
  movement::y0 = y0;
  movement::x1 = x1;
  movement::y1 = y1;
}
