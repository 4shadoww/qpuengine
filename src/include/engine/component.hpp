#ifndef QPU_COMPONENT_HPP
#define QPU_COMPONENT_HPP

#include <string>

class object;

class component{
protected:
  std::string name = "component";
  object* host;

public:
  component(std::string name);
  virtual ~component();
  virtual void init();
  virtual void update();
  virtual void fixedUpdate();
  void setHostObject_(object* ob);
  object* getHostObject();
  std::string getName();
  void setName(std::string name);
};

#endif
