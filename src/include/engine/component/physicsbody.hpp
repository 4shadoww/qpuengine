#ifndef QPU_PHYSICSBODY_HPP
#define QPU_PHYSICSBODY_HPP

#include <vector>

#include <SFML/System/Vector2.hpp>

#include "engine/component.hpp"

class physicsbody : public component{
public:
  enum bodytype {t_convex, t_circle};
private:
  bodytype type = t_convex;
  std::vector<sf::Vector2f> verticles;
  float dVec = 16.f;
  float radius = 16.f;

  float mass = 1.f;

  float velocity = 0.f;
  float last_velocity = 0.f;
  bool use_last_position = true;
  bool velocity_is_valid = false;

  sf::Vector2f phyen_pos;

  bool isstatic = false;
  bool istrigger = false;

  bool iscolliding = false;

  float size = 1.f;
  float rotation = 0.f;

  sf::Vector2f min;
  sf::Vector2f max;
  sf::Vector2f center;

  std::vector<object*> colliding;
public:
  bool use_phyen = false;

  physicsbody(std::string name);

  void init();
  std::vector<sf::Vector2f>& getVerticles();
  void setVerticles(std::vector<sf::Vector2f> verticles);
  void setVecticlePosition(int verticle, float x, float y);
  void setVecticlePosition(int verticle, sf::Vector2f vector);
  void removeVerticle(int i);
  unsigned int getVerticleCount();
  void setVerticleCount(int count);
  sf::Vector2f getVerticlePosition(int verticle);
  sf::Vector2f getVerticleLocalPosition(int verticle);
  void updateCenter();
  sf::Vector2f getCenter();
  sf::Vector2f getLocalCenter();
  sf::Vector2f getPosition();
  void setPosition(float x, float y);
  void setPosition(sf::Vector2f position);
  void move(float x, float y);
  void move(sf::Vector2f position);

  void setRadius(float radius);
  float getRadius();

  // Set body type
  void setType(bodytype type);

  // Get body type
  bodytype getType();

  // Static
  void setStatic(bool value);
  bool isStatic();

  // Trigger
  void setTrigger(bool value);
  bool isTrigger();

  // Rotation
  float getRotation();
  bool setRotation(float angle);
  bool rotate(float angle);

  // Size
  void setSize(float size);
  void setScale(float size);
  void scale(float size);
  float getSize();
  float getScale();

  // Colliding status
  bool isColliding();
  // Don't call this method
  void setColliding_(bool status);
  // Colliding list
  void clearColliding_();
  std::vector<object*> getColliding();
  bool isColliding(object* ob);
  bool isColliding(std::string name);
  bool isColliding(int id);

  // Update max min
  void updateMinMax();
  sf::Vector2f getMin();
  sf::Vector2f getMax();

  //! Get velocity
  float getVelocity();
  //! Set calculated velocity invalid
  /*! Don't call this method if you don't know what you are doing
      This is called automatically by phyen after update */
  void setVelocityInvalid_();
  //! Physics engine velocity
  float getPhyenVelocity_();
  void updatePhyenPosition_();
  void useLastPosition_(bool value);
  //! Get last velocity
  float getLastVelocity();
  //! Get acceleration
  float getAcceleration();

  //! Set mass
  void setMass(float mass);
  float getMass();
};

#endif
