#ifndef QPU_PINGEFF_HPP
#define QPU_PINGEFF_HPP

#include <string>

#include "engine/component.hpp"

class pingeff : public component{
private:
  float spent_time = 0.f;
  float m_time = 5.f;
  float max_radius = 10.f;
  bool destroy_after = false;
  bool active = true;
public:
  pingeff();
  pingeff(std::string name);
  void init();

  // Destroy after ping
  void setDestroyAfter(bool value);
  bool getDestroyAfter();

  // Ping time
  void setTime(float value);
  float getTime();

  // Max radius
  void setMaxRadius(float value);
  float getMaxRadius();

  void update();
};

#endif
