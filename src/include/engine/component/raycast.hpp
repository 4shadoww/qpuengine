#ifndef QPU_RAYCAST_HPP
#define QPU_RAYCAST_HPP

#include <cfloat>
#include <string>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Color.hpp>

#include "engine/component.hpp"

class physicsbody;
class object;
class linevertex;

class raycast : public component{
private:
  float angle = -90.f;
  float magnitude = 100.f;

  float distance = FLT_MAX;
  sf::Vector2f position = sf::Vector2f(FLT_MAX, FLT_MAX);
  physicsbody* collider = nullptr;

  // Debug
  bool debug = false;
  object* lineob;
  linevertex* line;
  sf::Color dcolor = sf::Color::Green;

public:
  raycast(std::string name);
  raycast();
  ~raycast();

  float getAngle();
  float getMagnitude();
  void updateRay_(float distance, sf::Vector2f position, physicsbody* collider);
  physicsbody* getHittingBody();
  bool isColliding();
  float getHitDistance();
  sf::Vector2f getHitPosition();

  bool setAngle(float angle);
  bool rotate(float angle);
  void setMagnitude(float magnitude);

  sf::Vector2f getOrigin();
  sf::Vector2f getEnd();

  void setDebug(bool value);
  void setDebugColor(sf::Color color);

  void updateRaycast();
  void update();
};

#endif
