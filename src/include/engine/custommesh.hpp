#ifndef QPU_CUSTOM_MESH_HPP
#define QPU_CUSTOM_MESH_HPP

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Texture.hpp>

class object;

class custommesh : public sf::Drawable, public sf::Transformable{
private:
  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
protected:
  object* host;
public:
  sf::VertexArray m_vertices;
  sf::Texture m_texture;
  void setHostObject_(object* host);
  virtual void create();
  virtual void update();
};

#endif
