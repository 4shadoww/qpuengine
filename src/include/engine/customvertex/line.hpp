#ifndef QPU_LINE_VERTEX_HPP
#define QPU_LINE_VERTEX_HPP

#include "engine/custommesh.hpp"

class linevertex : public custommesh{
private:
public:
  linevertex();
  void setPositions(sf::Vector2f first, sf::Vector2f second);
  void setPositions(float x0, float y0, float x1, float y1);
  void setFirstPosition(sf::Vector2f pos);
  void setFirstPosition(float x, float y);
  void setSecondPosition(sf::Vector2f pos);
  void setSecondPosition(float x, float y);
  void setColor(sf::Color color);
};

#endif
