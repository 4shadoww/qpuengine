#ifndef QPU_SIMPLE_PARTICLES_HPP
#define QPU_SIMPLE_PARTICLES_HPP

#include <vector>

#include "engine/custommesh.hpp"

class simple_particles : public custommesh{
private:
  class particle{
  public:
    sf::Vector2f position;
    sf::Vector2f velocity;
    sf::Color current_color = sf::Color::White;
    float life_time = 0.f;
    bool dead = false;
    float max_life_time;
    float color_r_step;
    float color_g_step;
    float color_b_step;
    float color_r = 250.f;
    float color_g = 250.f;
    float color_b = 250.f;
  };

  std::vector<particle> particles;

  bool created = false;
  bool global_space = false;
  bool update_angles = false;

  unsigned int quad_count = 200;
  unsigned int deadparticles = 0;
  float point_size = 2.f;
  float half_point = 1.f;

  float point_min_life_time = 0.3f;
  float point_max_life_time = 1.f;

  float min_angle = 0.f;
  float max_angle = 360.f;

  sf::Color start_color = sf::Color::White;
  sf::Color end_color = sf::Color::White;

  float radius = 16.f;

  float velocity_x = 3.f;
  float velocity_y = 3.f;

  float color_r_delta;
  float color_g_delta;
  float color_b_delta;

public:
  //! Set angles
  void setAngles(float min_angle, float max_angle);
  //! Set particle count
  void setParticleCount(unsigned int count);
  //! Set particle spawm radius
  void setSpawnRadius(float radius);
  //! Set min and max particle life time
  void setLifeTimes(float min, float max);
  //! Set min particle life time
  void setMinLifeTime(float time);
  //! Set max particle life time
  void setMaxLifeTime(float time);
  //! Set particle start and end color
  void setColors(sf::Color start, sf::Color end);
  //! Set particle start color
  void setStartColor(sf::Color color);
  //! Set particle end color
  void setEndColor(sf::Color color);
  void setSize(float size);
  void setVelocity(float velocity);
  void setVelocityX(float velocity);
  void setVelocityY(float velocity);
  void useGlobalSpace(bool value);
  float newLifeTime();
  void calculateStep(simple_particles::particle& par);
  void addParticles(unsigned int count);
  void initPositions();
  simple_particles::particle& getPos(unsigned int ind);
  void resetColor(simple_particles::particle& par);
  void updateColor(simple_particles::particle& par);
  void calculatePos(simple_particles::particle& par);
  void create();
  void updateParticleCount();
  void update();
};

#endif
