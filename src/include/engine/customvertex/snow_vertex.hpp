#ifndef QPU_SNOW_VERTEX_HPP
#define QPU_SNOW_VERTEX_HPP

#include <vector>

#include "engine/custommesh.hpp"

class particle{
public:
  sf::Vector2f position;
  sf::Vector2f velocity;
};

class snow_vertex : public custommesh{
private:
  bool created = false;
  std::vector<particle> particles;
  float point_size = 5.f;
  float half_point = 2.5f;
  unsigned int quad_count = 100;
  sf::Color color = sf::Color::White;

  int width = 800;
  int height = 600;
  float x_spawn = 800.f;
  float y_spawn = 600.f;
  float velocity_x = 5.f;
  float velocity_y = 10.f;
public:
  void initPositions();
  void create();
  void calculatePos(particle& par);
  void update();
};

#endif
