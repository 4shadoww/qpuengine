#ifndef QPU_DEBUG_HPP
#define QPU_DEBUG_HPP

#include <SFML/System/Vector2.hpp>

class object;

class debugger{
private:
  // Keys
  bool f1released = true;
  bool f2released = true;

  // Show fps
  bool show_stats = false;
  object* stats = nullptr;
  float last_fps = 0.f;

  // Debug draw
  bool showcolliders = false;
public:
  void update();

  // Fps counter
  void updateShowStats();
  void showStats(bool value);
  void statsUpdate();

  // Debug draw
  void updateDebugDraw();

  // Ping object
  void pingObject(object* ob);
  void pingPosition(sf::Vector2f pos);
  // Debug mark
  object* debugMark(float x, float y, float radius=8.f);
  //! Freeze engine (main thread) for milliseconds
  void freeze(int time=5000);
};

#endif
