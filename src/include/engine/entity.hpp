#ifndef QPU_ENTITY_HPP
#define QPU_ENTITY_HPP

class entity{
public:
  virtual ~entity();
  virtual void init();
  virtual void update();
  virtual void fixedUpdate();
};

#endif
