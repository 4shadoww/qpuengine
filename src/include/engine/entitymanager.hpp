#ifndef QPU_ENTITYMANAGER_HPP
#define QPU_ENTITYMANAGER_HPP

#include "engine/entity.hpp"

class entitymanager{
private:
  entity* currententity = nullptr;
  bool initialized = false;

public:
  entitymanager();
  void initEntity();
  void loadEntity(entity* newentity);
  void update();
  void fixedUpdate();
};

#endif
