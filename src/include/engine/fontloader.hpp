#ifndef FONTLOADER_HPP
#define FONTLOADER_HPP

#include <vector>
#include <string>

#include <SFML/Graphics/Font.hpp>

class fontloader{
private:
  std::vector<sf::Font*> fonts;

public:
  void loadFont(std::string path);
  sf::Font* getFont(std::string name);
};

#endif
