#ifndef QPU_INPUT_MANAGER_HPP
#define QPU_INPUT_MANAGER_HPP

#include <unordered_map>
#include <string>

#include <SFML/Window/Keyboard.hpp>

namespace sf{
  class RenderWindow;
}

class input_manager{
private:
  struct eveK{
    sf::Keyboard::Key key;
    eveK(sf::Keyboard::Key key){
      this->key = key;
    }
  };

  sf::RenderWindow* sfwindow;
  std::unordered_map<std::string, eveK> key_bindings;

public:
  input_manager();

  void clearLists();
  void loadDefaults();

  bool bindKey(sf::Keyboard::Key key, std::string name);

  bool keyPressed(sf::Keyboard::Key key, bool req_focus=true);
  bool keyPressed(std::string name, bool req_focus=true);

  bool isBinded(sf::Keyboard::Key key);
  bool isBinded(std::string name);
  bool isBinded(sf::Keyboard::Key key, std::string name);

  sf::Vector2i getMouseRawPos();
  sf::Vector2i getMousePos();
  sf::Vector2i getMousePos(sf::RenderWindow* window);
  sf::Vector2f getMouseGlobalPos();
  sf::Vector2f getMouseGlobalPos(sf::Vector2f relative);
  sf::Vector2f getMouseGlobalPos(sf::RenderWindow* window);
  sf::Vector2f getMouseGlobalPos(sf::RenderWindow* window, sf::Vector2f relative);
};

#endif
