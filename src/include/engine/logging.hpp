#ifndef QPU_LOGGING_HPP
#define QPU_LOGGING_HPP

#include <iostream>
#include <string>
#include <vector>

#include <SFML/System/Vector2.hpp>

class logger {
private:
  std::string name;
public:
  logger();
  logger(std::string name);

  std::string getName();

  // Print to stdout
  void stdout();
  template<typename First, typename ... Last>
  void stdout(First arg, const Last&... rest){
    std::cout << arg << " ";
    logger::stdout(rest...);
  }
  // stdout sf::Vector2f
  template<typename ... Last>
  void stdout(sf::Vector2f arg, const Last&... rest){
    std::cout << arg.x << ", " << arg.y << " ";
    logger::stdout(rest...);
  }
  // stdout uint8_t
  template<typename ... Last>
  void stdout(uint8_t arg, const Last&... rest){
    std::cout << unsigned(arg) << " ";
    logger::stdout(rest...);
  }
  // Print to stderr
  void stderr();
  template<typename First, typename ... Last>
  void stderr(First arg, const Last&... rest){
    std::cerr << arg << " ";
    logger::stderr(rest...);
  }
  // stderr sf::Vector2f
  template<typename ... Last>
  void stderr(sf::Vector2f arg, const Last&... rest){
    std::cerr << arg.x << ", " << arg.y << " ";
    logger::stderr(rest...);
  }
  // stderr uint8_t
  template<typename ... Last>
  void stderr(uint8_t arg, const Last&... rest){
    std::cout << unsigned(arg) << " ";
    logger::stderr(rest...);
  }
  // Info
  template<typename First, typename ... Last>
  void info(First arg, const Last&... rest) {
    stdout(arg, rest...);
  }
  // Error
  template<typename First, typename ... Last>
  void error(First arg, const Last&... rest) {
    stderr(arg, rest...);
  }
};

class logging {
private:
  std::vector<logger*> loggers;
public:
  logger* create(std::string name);
  logger* get(std::string name);
  bool destroy(std::string name);
};

#endif
