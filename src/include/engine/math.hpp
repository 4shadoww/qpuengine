#ifndef QPU_MATH_HPP
#define QPU_MATH_HPP

#include <SFML/System/Vector2.hpp>
#include <cfloat>
#include <cmath>

// Value of the pi
const long double PI = 3.141592653589793238L;

// Macros
#define degToRad(angleDegrees) (angleDegrees * PI / 180.f)
#define radToDeg(angleRadians) (angleRadians * 180.f / PI)

class object;

namespace qpumath{
  extern float smoothing(float current, float last, float smoothing=0.9f);
  extern float getAngle(sf::Vector2f delta);
  extern sf::Vector2f angleToVector(float angle);
  extern sf::Vector2f getEndPoint(sf::Vector2f start, float angle, float magnitude);
  extern sf::Vector2f getCircleAndLineIntersection(sf::Vector2f a, sf::Vector2f b, float angle, float magnitude, sf::Vector2f c, float radius);
  extern sf::Vector2f getCircleAndLineIntersection(sf::Vector2f a, float angle, float magnitude, sf::Vector2f c, float radius);
  extern sf::Vector2f getLinesIntersection(sf::Vector2f a, sf::Vector2f b, sf::Vector2f c, sf::Vector2f d);
  extern float getDistance(sf::Vector2f v0, sf::Vector2f v1);
  extern float dotproduct(sf::Vector2f v0, sf::Vector2f v1);
  extern sf::Vector2f getAxis(sf::Vector2f vec0, sf::Vector2f vec1);
  extern bool inSector(float angle, float start, float end);
  extern float invertAngle(float angle);
  extern float fixedAngle(float angle);
  extern float semiFixedAngle(float angle);
  extern float anglesDiff(float angle0, float angle1);
  extern float getMinAngleDiff(float angle0, float angle1);
}

#endif
