#ifndef QPU_OBJECT_HPP
#define QPU_OBJECT_HPP

#include <string>
#include <vector>

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Shader.hpp>

#include "engine/component/physicsbody.hpp"
#include "engine/custommesh.hpp"

class component;

class object{
private:
  //! Object's id
  int id = -1;
  //! Object's name
  std::string name;
  //! Object's position
  sf::Vector2f position = sf::Vector2f(0.0f, 0.0f);
  //! Holds object's position from previous frame
  /*! Can be used to calculate velocity */
  sf::Vector2f last_position = sf::Vector2f(0.0f, 0.0f);
  //! Hold object's position from before last_position
  sf::Vector2f before_last_position = sf::Vector2f(0.0f, 0.0f);
  //! Object's size
  float size = 1.f;
  //! Is marked to be deleted by object manager
  bool markedfordeletion = false;
  //! is enabled
  bool m_enabled = true;
  //! List of objects childs
  std::vector<object*> childs;
  //! Object's parent
  object* parent = nullptr;
  //! List of components
  std::vector<component*> components;
  int layer = 0;
  bool alwayslast = false;
  int view = 0;
  unsigned int loop;
  float rotation = 0.f;
  std::vector<std::string> tags;
  //! Shader
  sf::Shader* shader = nullptr;
  //! Blend mode
  sf::BlendMode blendmode = sf::BlendNone;
  //! Blend mode enabled
  bool blendmode_enabled = false;

public:
  enum meshtypes {t_none = -1, t_circle, t_convex, t_rectangle, t_sprite, t_text, t_custom};

  physicsbody* collider = nullptr;

  sf::CircleShape* circlemesh = nullptr;
  sf::ConvexShape* convexmesh = nullptr;
  sf::RectangleShape* rectanglemesh = nullptr;
  sf::Sprite* sprite = nullptr;
  sf::Text* textmesh = nullptr;
  custommesh* cumesh = nullptr;

  meshtypes meshtype;

  //! Constructor
  object(std::string name, int loop = 0);
  //! Destructor
  ~object();

  //! Update
  /*! Updates every frame */
  void update();
  //! Fixed update
  /*! Updates every physics update */
  void fixedUpdate();

  //! Create circle mesh
  /*! After construction accessible using "circlemesh" variable */
  void createCircleMesh();
  //! Create convex mesh
  /*! After construction accessible using "convexmesh" variable */
  void createConvexMesh();
  //! Create rectangle mesh
  void createRectangleMesh();
  //! Create sprite mesh
  void createSprite();
  //! Create text mesh
  void createTextMesh();
  //! Check does object have mesh
  bool hasMesh();
  //! Deallocate mesh
  void destroyMesh();
  //! Mark object for deletion
  void destroy();
  //! Check should this object be destroyed
  bool destroyThis();
  //! Check should this object be destroyed
  bool destroyed();
  //! Get object id
  int getId();
  //! Get object name
  std::string getName();
  //! Set object id
  /*! Warning don't call this method useless you know what you are doing */
  void setId_(int id);
  //! Set object's name
  void setName(std::string name);
  //! Get object's position
  sf::Vector2f getPosition();
  //! Get object's previous position
  /*! Get object's position from last frame */
  sf::Vector2f getLastPosition();
  //! Get previous last position
  sf::Vector2f getPreviousLastPosition();
  //! Update last position
  /*! Don't call this method useless you know what you are doing
      If it's used wrongly then physics will get broken */
  void updateLastPosition_();
  //! Set object's position
  void setPosition(float x, float y);
  //! Set object's position
  void setPosition(sf::Vector2f position);
  //! Move object
  void move(float x, float y);
  //! Move object
  void move(sf::Vector2f position);
  //! Update mesh position
  void updateMeshPosition();
  //! Set rotation
  bool setRotation(float angle);
  //! Rotate
  bool rotate(float angle);
  //! Set size
  void setSize(float size);
  //! Set scale aka size
  void setScale(float size);
  //! Scale
  void scale(float size);
  //! Get size
  float getSize();
  //! Get scale aka size
  float getScale();

  //! Enable this object
  void setEnabled(bool value);
  //! Check is this enabled
  bool enabled();

  // Parent and child methods
  void addChild(object* ob);
  void addChild(int id);
  void addChild(std::string name);
  std::vector<object*>& getChilds();
  object* getChild(std::string name);
  void addParent(object* ob);
  void addParent(int id);
  void addParent(std::string name);
  object* getParent();
  bool hasChild(object* ob);
  bool hasChild(int id);
  bool hasChild(std::string name);
  bool hasChild();
  bool hasParent(object* ob);
  bool hasParent(int id);
  bool hasParent(std::string name);
  bool hasParent();
  void removeChilds();
  void removeChild(object* ob);
  void removeChild(int id);
  void removeChild(std::string name);
  void removeParent();

  // Physics
  void createCollider();
  void destroyCollider();
  bool hasCollider();
  component* getCollider();

  // Component methods
  template<typename T>
  T* addComponent(T* com){
    com->setHostObject_(this);
    com->init();
    object::components.push_back(static_cast<component*>(com));
    return com;
  }
  void removeComponent(std::string name);
  void removeComponent(const char* name);

  template<typename T>
  void removeComponent(T* com0){
    component* com = static_cast<component*>(com0);
    for(std::vector<component*>::iterator it = object::components.begin(); it != object::components.end(); ++it){
      if(com == (*it)){
        delete com;
        object::components.erase(it);
        break;
      }
    }
  }
  void clearComponents();
  bool hasComponent();
  bool hasComponent(component* com);
  bool hasComponent(std::string name);
  component* getComponent(std::string name);

  // Layers
  bool setLayer(int i);
  int getLayer();
  bool isAlwaysLast();
  void setAlwaysLast(bool last);

  // Set view
  int getView();
  void setView(int i);

  // Custom mesh
  template<typename T>
  T* setCustomMesh(T* mesh){
    object::destroyMesh();
    object::meshtype = t_custom;
    object::cumesh = static_cast<custommesh*>(mesh);
    object::cumesh->setPosition(object::getPosition());
    object::cumesh->setHostObject_(this);
    return mesh;
  }

  // Loops
  void setLoopId(unsigned int id);
  unsigned int getLoopId();

  // Tags
  //! Add tag
  /*! Add tag to the object */
  bool addTag(std::string name);
  //! Remove tags
  /*! Remove tag from the object */
  bool removeTag(std::string name);
  //! Check does object have tag
  bool hasTag(std::string name);
  //! Get tag vector
  /*! Get tags */
  std::vector<std::string> getTags();
  //! Add shader
  sf::Shader* addShader(sf::Shader* shader);
  //! Check does object have shader
  bool hasShader();
  //! Get shader
  sf::Shader* getShader();
  //! Remove shader
  /*! Removes shader but will not deallocate it */
  bool removeShader();
  //! Removes shader and deallocates it
  bool deleteShader();
  //! Set blend mode enabled
  void setBlendModeEnabled(bool value);
  //! Get blend mode enabled
  bool getBlendModeEnabled();
  //! Set blend mode
  void setBlendMode(sf::BlendMode mode);
  //! Set blend mode to none
  void removeBlend();
  //! Get blend mode
  sf::BlendMode &getBlendMode();
};

#endif
