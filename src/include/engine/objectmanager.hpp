#ifndef QPU_OBJECTMANAGER_HPP
#define QPU_OBJECTMANAGER_HPP

#include <vector>
#include <string>

class object;

class objectmanager{
private:
  std::vector<object*> addobjects;
  std::vector<int> unusedids;
  std::vector<object*> allobjects;
  std::vector<object*> objectsbyname;
  int highestid = 0;
  int object_count = 0;

public:
  std::vector<object*> objects;
  int registerObject(object* ob);
  object* createObject(std::string name = "default", int loop = 0);
  object* getObjectById(int id);
  object* getObjectByName(std::string name);
  std::vector<object*>& getObjectsByName(std::string name);
  void optimizeUnusedIds();
  int getNewHighestId();
  int getUnusedId();
  int generateId();
  void update();
  void updateObjects();
  void fixedUpdateObjects();
  std::vector<object*>& getObjects();
  void destroyAllByName(std::string name);
  int getObjectCount();
};

#endif
