#ifndef QPU_PHYSICS_HPP
#define QPU_PHYSICS_HPP

#include <vector>
#include <cfloat>

#include <SFML/System/Vector2.hpp>

class physicsbody;
class object;
class raycast;

struct projection{
  float min, max;
};

struct gapaxis{
  float gap = 0.f;
  sf::Vector2f axis = sf::Vector2f(0.f, 0.f);
};

struct rayin{
  float distance = FLT_MAX;
  sf::Vector2f position = sf::Vector2f(FLT_MAX, FLT_MAX);
  physicsbody* collider = nullptr;
};

class otphysics{
private:
  bool debugdraw;
  void createDebugMeshes();
  void createDebugMesh(object* ob);
  void destroyDebugMeshes();
public:
  otphysics();

  void update();

  //! Should do accurate collision detection
  /*! Check are objects possibly colliding using aabb */
  bool shouldCheck(object* ob0, object* ob1);
  //! Get min gap
  /*! Check collision for all objects and get min gap */
  float getGap(object* ob0, bool dynamic=true);
  //! Apply mtv
  void applyMTV(physicsbody* sq0, physicsbody* sq1, gapaxis &min);
  //! Collision check
  float checkCollision(object* ob0, object* ob1);
  //! Collision check for convex shapes
  float convexCollision(object* ob0, object* ob1);
  //! Collision check for circle and convex shape
  float circleConvexCollision(object* ob0, object* ob1);
  gapaxis projectOnCircle(physicsbody* sq0, physicsbody* sq1);
  gapaxis projectToCircle(physicsbody* sq0, physicsbody* sq1);
  //! Collision check for circles
  float circleCollision(object* ob0, object* ob1);
  gapaxis circleProjection(physicsbody* sq0, physicsbody* sq1);

  gapaxis projectOnAxes(physicsbody* sq0, physicsbody* sq1);
  float getGap(projection gap0, projection gap1);
  projection project(sf::Vector2f axis, physicsbody* sq);
  projection projectCircle(sf::Vector2f axis, sf::Vector2f vec0, sf::Vector2f vec1);
  sf::Vector2f getAxis(sf::Vector2f vec0, sf::Vector2f vec1);

  // Raycast methods
  bool inRange(object* ob0, object* ob1, float magnitude);
  rayin convexRayIntersection(sf::Vector2f &origin, sf::Vector2f &end, physicsbody* col);
  rayin circleRayIntersection(sf::Vector2f &origin, sf::Vector2f &end, physicsbody* col, raycast* ray);
  rayin rayIntersection(sf::Vector2f &origin, sf::Vector2f &end, object* ob, raycast* ray);
  void updateRaycast(raycast* ray);

  // Debug draw
  bool shouldUpdate(object* ob0, object* ob1);
  void updateDebugMesh(object* ob0, object* ob1);
  void enableDebugDraw();
  void disableDebugDraw();
  bool debugDrawEnabled();
  void debugDraw();
};

#endif
