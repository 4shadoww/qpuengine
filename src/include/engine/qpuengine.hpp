#ifndef QPUENGINE_HPP
#define QPUENGINE_HPP

#include "engine/window.hpp"
#include "engine/renderer.hpp"
#include "engine/objectmanager.hpp"
#include "engine/entitymanager.hpp"
#include "engine/physics.hpp"
#include "engine/fontloader.hpp"
#include "engine/input_manager.hpp"
#include "engine/qpuloop.hpp"
#include "engine/logging.hpp"
#include "engine/debugger.hpp"

namespace qput{
  typedef debugger debugger;
}

class qpuengine{
private:
  static qpuengine* instance;
  qpuengine();
  std::string path;


public:
  static qpuengine* getInstance();

  window* wm;
  renderer* re;
  objectmanager* objmgr;
  entitymanager* entitymgr;
  otphysics* phyen;
  fontloader* fontmgr;
  input_manager* inputmgr;
  logging* qpulog;
  qput::debugger* debugger;

  logger* debug;

  void init();
  void startLoop();
  float deltaTime();
  float fixedDeltaTime();
  std::string getPath();
  bool hasFocus();

  // Shorcuts
  // Loops
  bool addLoop(unsigned int id, bool enabled);
  qpuloop* getLoop(unsigned int id);
  bool loopEnabled(unsigned int id);
  bool loopExists(unsigned int id);
  bool removeLoop(unsigned int id);
  object* createObject(std::string name = "default", int loop = 0);


  // Input shorcuts
  bool keyPressed(sf::Keyboard::Key key);
  bool keyPressed(std::string name);

  sf::Vector2i getMouseRawPos();
  sf::Vector2i getMousePos();
  sf::Vector2i getMousePos(sf::RenderWindow* window);
  sf::Vector2f getMouseGlobalPos();
  sf::Vector2f getMouseGlobalPos(sf::Vector2f relative);
  sf::Vector2f getMouseGlobalPos(sf::RenderWindow* window);
  sf::Vector2f getMouseGlobalPos(sf::RenderWindow* window, sf::Vector2f relative);

};

#endif
