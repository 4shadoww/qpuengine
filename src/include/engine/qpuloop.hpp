#ifndef QPU_QPULOOP_HPP
#define QPU_QPULOOP_HPP

class qpuloop{
public:
  unsigned int id;
  bool enabled;
  qpuloop(unsigned int id);
  qpuloop(unsigned int id, bool enabled);
};

#endif
