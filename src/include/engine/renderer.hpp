#ifndef QPU_RENDERER_HPP
#define QPU_RENDERER_HPP

#include <vector>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

class qpuloop;
class object;

class renderer{
private:
  sf::RenderWindow* sfwindow;
  std::vector<int> layers;
  std::vector<sf::View*> views;
  unsigned int framerate = 60.f;

public:
  sf::Time deltatime;
  sf::Time fixeddeltatime;
  sf::View* camera;
  sf::View* hudcamera;
  std::vector<qpuloop*> loops;

  renderer(sf::RenderWindow* sfwindow);
  bool render();
  void drawObject(object* ob);
  void drawObjectWithBlend(object* ob);
  void drawObjectWithShader(object* ob);
  void drawObjects();
  void moveCam(float x, float y);
  void moveCam(sf::Vector2f vec);
  void setCamPosition(float x, float y);
  void setCamPosition(sf::Vector2f vec);
  sf::Vector2f getCamPosition();
  // Layers
  bool addLayer(int i);
  bool removeLayer(int i);
  bool hasLayer(int i);
  std::vector<int>& getLayers();
  // Views
  sf::View* getView(int index);
  int addView(sf::View* view);
  void removeView(sf::View* view);
  void removeView(int index);
  // Loops
  bool addLoop(unsigned int id, bool enabled);
  qpuloop* getLoop(unsigned int id);
  bool loopEnabled(unsigned int id);
  bool loopExists(unsigned int id);
  bool removeLoop(unsigned int id);
  //! Get framerate
  unsigned int getFramerate();
  //! Set framerate
  void setFramerate(unsigned int fr);
};

#endif
