#ifndef QPU_WINDOW_HPP
#define QPU_WINDOW_HPP

#include <string>

#include <SFML/Graphics/RenderWindow.hpp>

class window{
private:
  int width;
  int height;
  std::string title;
  bool hasfocus = false;


public:
  ~window();
  window(int width, int height, std::string title);
  window(int width, int height);
  window(std::string title);
  window();

  int getWidth();
  int getHeight();
  sf::Vector2i getSize();

  sf::RenderWindow* sfwindow;
  int create();
  void setFocus_(bool value);
  bool hasFocus();
};

#endif
