#ifndef TESTENTITY_HPP
#define TESTENTITY_HPP

#include "engine/entity.hpp"
class object;
class nonplayer;
class raycast;

class testentity : public entity{
private:
  object* ob0;
  object* ob1;
public:
  void init();
  void fixedUpdate();
};

#endif
