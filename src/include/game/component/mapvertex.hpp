#ifndef MAPVERTEX_HPP
#define MAPVERTEX_HPP

#include <json/json.h>

#include "engine/custommesh.hpp"

class mapvertex : public custommesh{
public:
  int numberOfTiles(Json::Value &data);
  bool load(sf::Texture* texture, int tilewidth, Json::Value &data, unsigned int width, unsigned int height, sf::Vector2f offset);
};

#endif
