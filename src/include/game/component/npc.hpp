#ifndef NPC_HPP
#define NPC_HPP

#include <string>

#include "engine/component.hpp"
#include "game/pathfinder/agent.hpp"

class pathfinden;

class nonplayer : public component{
private:
  float velocity = 5.f;
  unsigned int current_node = 0;
  pathfinder agent;
  bool path_found = false;
public:
  nonplayer(std::string name);
  nonplayer();

  void fixedUpdate();

  bool move(float x, float y);
  bool move(sf::Vector2f position);
  bool move2Node(int x, int y);

  // Agent methods
  void setEnviroment(pathfinden* env);
  void setCollider(object* ob);
  void enableCollision(bool value);
  void debug();
  sf::Vector2i getClosestNode(sf::Vector2f position);
  sf::Vector2i getClosestNode(int x, int y);
};

#endif
