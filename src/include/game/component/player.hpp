#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>

#include "engine/component.hpp"

class physicsbody;
class spell_caster;

class player : public component{
private:
  spell_caster* caster;

public:
  player(std::string name);

  void init();
  void fixedUpdate();

};

#endif
