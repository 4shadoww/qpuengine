#ifndef SIMPLE_ENEMY_HPP
#define SIMPLE_ENEMY_HPP

#include <string>
#include "engine/component.hpp"

class sight;

class simple_enemy : public component{
private:
sight* vision;

public:
  simple_enemy(std::string name);
  simple_enemy();
  ~simple_enemy();

  void init();
  void fixedUpdate();
};

#endif
