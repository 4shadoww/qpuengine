#ifndef ELEMENT_HPP
#define ELEMENT_HPP

class element{
public:
  enum etype {fire, frost, air, earth, water, ice, mist, infinity};
  etype type;
  bool uneven = false;
  bool main_negative = false;
  bool infinity_negative = false;

  element();
  element(etype type, bool uneven=false, bool negative=false);
};

#endif
