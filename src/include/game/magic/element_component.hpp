#ifndef ELEMENT_COMPOMENT_HPP
#define ELEMENT_COMPOMENT_HPP

#include <string>
#include <vector>

#include <SFML/System/Vector2.hpp>

#include "engine/component.hpp"
#include "game/magic/element.hpp"
#include "game/magic/element_hold.hpp"

class element_comp : public component{
private:
  float radius_factor = 0.25f;
  float radius = 0.f;
  float energy = 0.f;
  sf::Vector2f velocity;
  std::vector<element_hold> elements;
  bool skip_next = false;
public:
  element_comp(std::string name);
  element_comp();

  void init();
  void addElement(element ele, float count = 1);
  void addVelocity(float velocity);
  void addVelocity(float velocity, sf::Vector2f heading);
  void updateRadius();

  void setRadiusFactor(float rf);
  float getRadiusFactor();

  void drainElements();
  bool overValue(float first, float second, float value);
  void reduceVelocity();

  void fixedUpdate();
};

#endif
