#ifndef ELEMENT_HOLD_HPP
#define ELEMENT_HOLD_HPP

#include "game/magic/element.hpp"

struct element_hold{
  float m_count = 1.f;
  element m_element;
  element_hold(element ele, float count = 1);
};

#endif
