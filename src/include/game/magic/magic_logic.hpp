#ifndef MAGIC_ENVIROMENT_HPP
#define MAGIC_ENVIROMENT_HPP

#include <vector>

#include "game/magic/element_hold.hpp"
#include "game/magic/element.hpp"

class element_comp;

struct func_resolver{
  int first;
  int second;
  bool (*func)(element_hold&, element_hold&);
  func_resolver(int first, int second, bool (*func)(element_hold&, element_hold&));
};

namespace magic_logic{
  extern element_comp* comp;

  extern std::vector<func_resolver> func_table;

  // Reactions
  extern bool infinityAny(element_hold& inf, element_hold& norm);

  extern bool fireAndFrost(element_hold& fire, element_hold& frost);
  extern bool fireAndAir(element_hold& fire, element_hold& air);
  extern bool fireAndEarth(element_hold& fire, element_hold& earth);
  extern bool fireAndWater(element_hold& fire, element_hold& water);
  extern bool fireAndIce(element_hold& fire, element_hold& ice);
  extern bool fireAndMist(element_hold& fire, element_hold& mist);

  extern bool frostAndAir(element_hold& frost, element_hold& air);
  extern bool frostAndEarth(element_hold& frost, element_hold& earth);
  extern bool frostAndWater(element_hold& frost, element_hold& water);
  extern bool frostAndIce(element_hold& frost, element_hold& ice);
  extern bool frostAndMist(element_hold& frost, element_hold& mist);

  extern bool airAndEarth(element_hold& air, element_hold& earth);
  extern bool airAndWater(element_hold& air, element_hold& water);
  extern bool airAndIce(element_hold& air, element_hold& ice);
  extern bool airAndMist(element_hold& air, element_hold& mist);

  extern bool earthAndWater(element_hold& earth, element_hold& water);
  extern bool earthAndIce(element_hold& earth, element_hold& ice);
  extern bool earthAndMist(element_hold& earth, element_hold& mist);

  extern bool waterAndIce(element_hold& water, element_hold& ice);
  extern bool waterAndMist(element_hold& water, element_hold& mist);

  extern bool iceAndMist(element_hold& ice, element_hold& mist);
  // Reactions end

  extern bool elementReaction(element_hold& ele0, element_hold& ele1, element_comp* comp);
};

#endif
