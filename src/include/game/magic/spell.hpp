#ifndef SPELL_HPP
#define SPELL_HPP

#include <vector>

#include "game/magic/element.hpp"

class spell{
public:
  struct node{
    enum node_type {create, wait};
    node_type action_type;

    // Create node
    element::etype type;
    bool uneven = false;
    bool negative = false;
    float velocity;
    float energy = 0;
    float count = 1;

    // Wait node
    float sleep;

    node(node_type action_type, element::etype type, float count, bool uneven=false, bool negative=false, float velocity = 0.f, float energy = 5);
    node(node_type type, float sleep);
  };

  std::vector<std::vector<node>> instructions;
};

#endif
