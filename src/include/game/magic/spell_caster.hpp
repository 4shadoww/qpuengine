#ifndef SPELL_CASTER_HPP
#define SPELL_CASTER_HPP

#include <SFML/System/Vector2.hpp>

#include "engine/component.hpp"
#include "game/magic/spell.hpp"

class spell;
class element_comp;

class spell_caster : public component{
private:
  bool casting = false;
  spell* spell_to_cast;
  unsigned int current_pos = 0;
  bool sleeping = false;
  float slept = 0.f;
  sf::Vector2f heading;
  float angle = 0.f;
  float distance = 0.f;
public:
  spell_caster(std::string name);
  spell_caster();

  void init();
  sf::Vector2f getHostAngle();
  void castSpell(spell* mag_spell);
  void castSpell(spell* mag_spell, float angle);
  void setDistance(float distance);
  float getDistance();
  void createElement(spell::node& node, element_comp* elfus);
  void fixedUpdate();
};

#endif
