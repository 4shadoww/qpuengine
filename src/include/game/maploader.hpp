#ifndef MAPLOADER_HPP
#define MAPLOADER_HPP

#include <string>

#include <json/json.h>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Texture.hpp>

class object;

class spriteloader{
private:
  int width = 32;
  int height = 32;
  sf::Image spritesrc;
  std::vector<sf::Texture*> sprites;
public:
  bool loadSprites(std::string name);
  void generateSprites();
  void setSpriteSize(int width, int height);
  sf::Texture* getSprite(int id);
};

class maploader{
private:
  int width;
  int height;
  int offsetx;
  int offsety;
  int tilewidth = 32;
  spriteloader spriteload;
  object* maproot;
public:
  maploader();
  void loadConfig(Json::Value &json);
  object* createPolygon(Json::Value &data);
  object* createEllipse(Json::Value &data);
  object* createCube(Json::Value &data);
  void loadPhysics(Json::Value &data);
  bool loadMap(std::string mapjson, int x, int y);
};

#endif
