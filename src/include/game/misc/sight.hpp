#ifndef SIGHT_HPP
#define SIGHT_HPP

#include <vector>

class object;
class raycast;
class linevertex;

class sight{
private:
  //! Vision range
  float distance = 200.f;
  //! Field of view
  float fov = 120.f;
  //! Fov direction
  float facing_angle = 0.f;
  //! Raycast test
  unsigned int raycast_count = 5;

  // Min max fov
  float fov_min;
  float fov_max;

  bool debug = false;

  // Raycast
  raycast* ray;

  //! Host object
  object* host;

  // Debug
  object* min_ob;
  object* max_ob;
  linevertex* min_line;
  linevertex* max_line;

public:
  std::vector<object*> objects;

  sight(object* host);
  ~sight();

  void update();

  bool convexInSector(object* ob);
  bool circleInSector(object* ob);
  bool testConvex(object* ob);
  bool testCircle(object* ob);

  void reCalculateMinMax();

  void setFov(float fov);
  float getFov();
  void setFacingAngle(float angle);
  float getFacingAngle();
  void setDebug(bool value);
  bool isDebugEnabled();
  void updateDebugDraw();

  std::vector<object*> &getSeenObjects();
};

#endif
