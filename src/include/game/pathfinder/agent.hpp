#ifndef PATHFINDER_AGENT_HPP
#define PATHFINDER_AGENT_HPP

#include <vector>

#include <SFML/System/Vector2.hpp>
#include "game/pathfinder/node.hpp"
#include "engine/object.hpp"

class pathfinden;
class pathnode;
class nodevec;

class pathfinder{
private:
  pathfinden* enviroment = nullptr;
  int width, height;
  int node_width, node_height;

  std::vector<pathnode*> open_list;
  std::vector<pathnode*> closed_list;

  pathnode* start = nullptr;
  pathnode* end = nullptr;

  object* testob = nullptr;

  bool enable_collision = false;
public:
  std::vector<pathnode*> path;

  pathfinder(pathfinden* enviroment);
  pathfinder();
  ~pathfinder();

  void setEnviroment(pathfinden* enviroment);

  sf::Vector2i getClosestNode(float x, float y);
  sf::Vector2i getClosestNode(sf::Vector2f position);

  int getHeuristic(pathnode* start, pathnode* end);
  int getMovementCost(pathnode* node);
  void calculateValues(pathnode* node);

  void findNodes(pathnode* position);
  pathnode* updateClosed();
  void findPath(pathnode* end);

  bool calculatePath(pathnode start, pathnode end);
  bool calculatePath(int start_x, int start_y, int end_x, int end_y);
  bool calculatePath(sf::Vector2i start, sf::Vector2i end);

  // Should reparent
  bool shouldReparent(pathnode* node0, pathnode* node1);

  // Clear lists
  void clearLists();

  // In lists
  bool inOpenList(int x, int y);
  bool inClosedList(int x, int y);

  // Get from open list
  pathnode* getOpenNode(int x, int y);

  // Get from closed list
  pathnode* getClosedNode(int x, int y);

  // Position is in the navigation space
  bool inRange(pathnode position);
  bool inRange(int x, int y);
  bool inRange(sf::Vector2f position);
  bool inRange(pathnode* position);
  bool xInRange(int x);
  bool yInRange(int y);

  // Collider methods
  void setCollider(object* ob);
  object* getCollider();
  void removeCollider();

  // Collision tests
  bool isSafe(int x0, int y0, int x1, int y1, bool ig_hv = false);

  // Node position
  sf::Vector2f getNodePosition(int i);

  // Enable collision detection
  void enableCollision(bool value);
  bool collisionEnabled();

  void debug();
};

#endif
