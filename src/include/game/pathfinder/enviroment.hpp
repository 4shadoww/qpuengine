#ifndef ASENVIROMENT_HPP
#define ASENVIROMENT_HPP

#include <vector>

class nodevec;
class movement;
class object;

class pathfinden{
private:
  int width, height;
  int node_width, node_height;
  object* testob = nullptr;
public:
  std::vector<nodevec> nodes;
  std::vector<movement> movements;

  pathfinden();
  pathfinden(int width, int height, int node_width, int node_height);

  // Get settings
  int getWidth();
  int getHeight();
  int getNodeWidth();
  int getNodeHeight();

  // Generate enviroment
  void generateUnsafeNodes();
  void generateUnsafeMovements();
  void generateEnviroment();
  std::vector<nodevec>& getNodes();
  bool ignoredNode(int x, int y);
  bool ignoredMovement(int x0, int y0, int x1, int y1);
  bool inRange(int x, int y);
  bool isSafe(int x0, int y0, int x1, int y1, bool ig_hv=false);
  void createTestCollider();
  void debug();
};

#endif
