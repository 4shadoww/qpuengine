#ifndef PF_NODE_HPP
#define PF_NODE_HPP

class nodevec{
public:
  nodevec();
  int x;
  int y;
  nodevec(int x, int y);
};

class pathnode{
public:
  pathnode();
  int x, y;
  int h, g, f;
  pathnode* parent = nullptr;
  pathnode(int x, int y);
};

class movement{
public:
  movement();
  int x0, y0;
  int x1, y1;
  movement(int x0, int y0, int x1, int y1);
};

#endif
