#include "engine/globals.hpp"

int main(int argc, char **argv){
    engine->init();
    engine->startLoop();

    return 0;
}
